USE [DragonWriterPro]
GO
/****** Object:  StoredProcedure [dbo].[Module_AddModule]    Script Date: 11/14/2014 2:31:04 AM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[Module_AddModule]
	@ID [uniqueidentifier],
	@ModulePath [nvarchar](max),
	@Namespace [nvarchar](max),
	@ProjectID [uniqueidentifier],
	@CreatedOn [datetime]
AS
BEGIN
Declare @IDCount [int];

SELECT @IDCount = COUNT([ID]) FROM [dbo].[Module]
WHERE [ID] = @ID

IF @IDCount = 0
	BEGIN
		INSERT INTO [dbo].[Module]
			(
				[ID]
				,[ModulePath]
				,[Namespace]
				,[ProjectID]
				,[CreatedOn]
				)
			VALUES(@ID
				,@ModulePath
				,@Namespace
				,@ProjectID
				,@CreatedOn
			)
	END
END
GO
/****** Object:  StoredProcedure [dbo].[Module_DeleteModule]    Script Date: 11/14/2014 2:31:04 AM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[Module_DeleteModule]
	@ID [uniqueidentifier]
AS
BEGIN
SET NOCOUNT ON
	DELETE
		FROM [dbo].[Module] 
	WHERE ID = @ID
END
GO
/****** Object:  StoredProcedure [dbo].[Module_EditModule]    Script Date: 11/14/2014 2:31:04 AM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[Module_EditModule]
	@ID [uniqueidentifier],
	@ModulePath [nvarchar](max),
	@Namespace [nvarchar](max),
	@ProjectID [uniqueidentifier],
	@CreatedOn [datetime]
AS
BEGIN
Declare @IDCount [int];

SELECT @IDCount = COUNT([ID]) FROM [dbo].[Module]
WHERE [ID] = @ID

IF @IDCount > 0
	BEGIN
		UPDATE [dbo].[Module]
			SET
				[ModulePath]=@ModulePath
				,[Namespace]=@Namespace
				,[ProjectID]=@ProjectID
				,[CreatedOn]=@CreatedOn
			WHERE [ID]=@ID			
	END
END
GO
/****** Object:  StoredProcedure [dbo].[Module_GetAllModule]    Script Date: 11/14/2014 2:31:04 AM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[Module_GetAllModule]
AS
BEGIN
SET NOCOUNT ON
	SELECT *
	FROM [Module]
END
GO
/****** Object:  StoredProcedure [dbo].[Module_GetAllPagedModule]    Script Date: 11/14/2014 2:31:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[Module_GetAllPagedModule]
	@CurrentPageIndex [int],
	@PageSize [int],
	@SortOrder [nvarchar](500),
	@ID [uniqueidentifier] = Null,
	@ModulePath [nvarchar](max) = Null,
	@Namespace [nvarchar](max) = Null,
	@ProjectID [uniqueidentifier] = Null,
	@CreatedOn [datetime] = Null
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @sql nvarchar(max),
	@paramlist  nvarchar(4000),
	@CurrentRowIndex int;

	SET @CurrentRowIndex = ((@CurrentPageIndex - 1) * @PageSize) + 1;

	SET @sql='
		WITH Custom_PageSegment AS (
			SELECT *,
			ROW_NUMBER() OVER ( ORDER BY ' + @SortOrder + ' ) as RowIndex
			FROM [dbo].[Module] sm
			WHERE 1=1'

		IF @ID IS NOT NULL
		SET @sql = @sql + ' AND sm.ID= @xID'
		IF @ModulePath IS NOT NULL
		SET @sql = @sql + ' AND sm.ModulePathLIKE ''%'' + @xModulePath+ ''%'''
		IF @Namespace IS NOT NULL
		SET @sql = @sql + ' AND sm.NamespaceLIKE ''%'' + @xNamespace+ ''%'''
		IF @ProjectID IS NOT NULL
		SET @sql = @sql + ' AND sm.ProjectID= @xProjectID'
		IF @CreatedOn IS NOT NULL
		SET @sql = @sql + ' AND sm.CreatedOn= @xCreatedOn'
		
SET @sql = @sql +
		')
		SELECT *
		FROM Custom_PageSegment
			WHERE
			RowIndex BETWEEN '
			+  CONVERT(nvarchar(10),@CurrentRowIndex)
			+ ' AND (' 
			+ CONVERT(nvarchar(10),@CurrentRowIndex) + ' + ' + CONVERT(nvarchar(10),@PageSize)
			+ ') - 1  order by '
			+ CONVERT(nvarchar(500),@SortOrder);
	SELECT @paramlist = 
		'@xID[uniqueidentifier]
		,@xModulePath[nvarchar](max)
		,@xNamespace[nvarchar](max)
		,@xProjectID[uniqueidentifier]
		,@xCreatedOn[datetime]
		'EXEC sp_executesql @sql, @paramlist, 
			@ID
			,@ModulePath
			,@Namespace
			,@ProjectID
			,@CreatedOn
END
GO
/****** Object:  StoredProcedure [dbo].[Module_GetModule]    Script Date: 11/14/2014 2:31:04 AM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[Module_GetModule]
	@ID [uniqueidentifier]
AS
BEGIN
SET NOCOUNT ON
	SELECT *
	FROM [dbo].[Module]
	WHERE ID = @ID
END
GO
/****** Object:  StoredProcedure [dbo].[Module_GetModuleByID]    Script Date: 11/14/2014 2:31:04 AM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[Module_GetModuleByID]
	@ID [uniqueidentifier]
AS
BEGIN
SET NOCOUNT ON
	SELECT *
	FROM [dbo].[Module]
	WHERE ID = @ID
END
GO
/****** Object:  StoredProcedure [dbo].[Module_TotalCountModule]    Script Date: 11/14/2014 2:31:04 AM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[Module_TotalCountModule]
	@TotalCount [int] OUTPUT
AS
BEGIN
SET NOCOUNT ON
	SELECT
	@TotalCount = COUNT(*)
	FROM [dbo].[Module]
END
GO
/****** Object:  StoredProcedure [dbo].[Module_TotalCountWithSearchModule]    Script Date: 11/14/2014 2:31:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[Module_TotalCountWithSearchModule]
	@TotalCount [int] OUTPUT,
	@ID [uniqueidentifier] = Null,
	@ModulePath [nvarchar](max) = Null,
	@Namespace [nvarchar](max) = Null,
	@ProjectID [uniqueidentifier] = Null,
	@CreatedOn [datetime] = Null
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @sql nvarchar(max),
	@paramlist  nvarchar(4000),
	@xTotalCount int

	SET @sql='SELECT @xTotalCount = COUNT(*)FROM [dbo].[Follow] sm WHERE 1=1'IF @ID IS NOT NULL
		SET @sql = @sql + ' AND sm.ID= @xID'
		IF @ModulePath IS NOT NULL
		SET @sql = @sql + ' AND sm.ModulePathLIKE ''%'' + @xModulePath+ ''%'''
		IF @Namespace IS NOT NULL
		SET @sql = @sql + ' AND sm.NamespaceLIKE ''%'' + @xNamespace+ ''%'''
		IF @ProjectID IS NOT NULL
		SET @sql = @sql + ' AND sm.ProjectID= @xProjectID'
		IF @CreatedOn IS NOT NULL
		SET @sql = @sql + ' AND sm.CreatedOn= @xCreatedOn'
		SELECT @paramlist = '@xTotalCount [int] OUTPUT, 
		@xID[uniqueidentifier]
		,@xModulePath[nvarchar](max)
		,@xNamespace[nvarchar](max)
		,@xProjectID[uniqueidentifier]
		,@xCreatedOn[datetime]
		'EXEC sp_executesql @sql, @paramlist, @xTotalCount OUTPUT,
			@ID
			,@ModulePath
			,@Namespace
			,@ProjectID
			,@CreatedOn
END
GO
/****** Object:  StoredProcedure [dbo].[Project_AddProject]    Script Date: 11/14/2014 2:31:04 AM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[Project_AddProject]
	@ID [uniqueidentifier],
	@WebPath [nvarchar](max),
	@DatabaseServer [nvarchar](max),
	@DBUserName [nvarchar](max),
	@Pass [nvarchar](max),
	@UserID [uniqueidentifier],
	@CreatedOn [datetime],
	@DatabaseName [nvarchar](max)
AS
BEGIN
Declare @IDCount [int];

SELECT @IDCount = COUNT([ID]) FROM [dbo].[Project]
WHERE [ID] = @ID

IF @IDCount = 0
	BEGIN
		INSERT INTO [dbo].[Project]
			(
				[ID]
				,[WebPath]
				,[DatabaseServer]
				,[DBUserName]
				,[Pass]
				,[UserID]
				,[CreatedOn]
				,[DatabaseName]
				)
			VALUES(@ID
				,@WebPath
				,@DatabaseServer
				,@DBUserName
				,@Pass
				,@UserID
				,@CreatedOn
				,@DatabaseName
			)
	END
END
GO
/****** Object:  StoredProcedure [dbo].[Project_DeleteProject]    Script Date: 11/14/2014 2:31:04 AM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[Project_DeleteProject]
	@ID [uniqueidentifier]
AS
BEGIN
SET NOCOUNT ON
	DELETE
		FROM [dbo].[Project] 
	WHERE ID = @ID
END
GO
/****** Object:  StoredProcedure [dbo].[Project_EditProject]    Script Date: 11/14/2014 2:31:04 AM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[Project_EditProject]
	@ID [uniqueidentifier],
	@WebPath [nvarchar](max),
	@DatabaseServer [nvarchar](max),
	@DBUserName [nvarchar](max),
	@Pass [nvarchar](max),
	@UserID [uniqueidentifier],
	@CreatedOn [datetime],
	@DatabaseName [nvarchar](max)
AS
BEGIN
Declare @IDCount [int];

SELECT @IDCount = COUNT([ID]) FROM [dbo].[Project]
WHERE [ID] = @ID

IF @IDCount > 0
	BEGIN
		UPDATE [dbo].[Project]
			SET
				[WebPath]=@WebPath
				,[DatabaseServer]=@DatabaseServer
				,[DBUserName]=@DBUserName
				,[Pass]=@Pass
				,[UserID]=@UserID
				,[CreatedOn]=@CreatedOn
				,[DatabaseName]=@DatabaseName
			WHERE [ID]=@ID			
	END
END
GO
/****** Object:  StoredProcedure [dbo].[Project_GetAllPagedProject]    Script Date: 11/14/2014 2:31:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[Project_GetAllPagedProject]
	@CurrentPageIndex [int],
	@PageSize [int],
	@SortOrder [nvarchar](500),
	@ID [uniqueidentifier] = Null,
	@WebPath [nvarchar](max) = Null,
	@DatabaseServer [nvarchar](max) = Null,
	@DBUserName [nvarchar](max) = Null,
	@Pass [nvarchar](max) = Null,
	@UserID [uniqueidentifier] = Null,
	@CreatedOn [datetime] = Null,
	@DatabaseName [nvarchar](max) = Null
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @sql nvarchar(max),
	@paramlist  nvarchar(4000),
	@CurrentRowIndex int;

	SET @CurrentRowIndex = ((@CurrentPageIndex - 1) * @PageSize) + 1;

	SET @sql='
		WITH Custom_PageSegment AS (
			SELECT *,
			ROW_NUMBER() OVER ( ORDER BY ' + @SortOrder + ' ) as RowIndex
			FROM [dbo].[Project] sm
			WHERE 1=1'

		IF @ID IS NOT NULL
		SET @sql = @sql + ' AND sm.ID= @xID'
		IF @WebPath IS NOT NULL
		SET @sql = @sql + ' AND sm.WebPathLIKE ''%'' + @xWebPath+ ''%'''
		IF @DatabaseServer IS NOT NULL
		SET @sql = @sql + ' AND sm.DatabaseServerLIKE ''%'' + @xDatabaseServer+ ''%'''
		IF @DBUserName IS NOT NULL
		SET @sql = @sql + ' AND sm.DBUserNameLIKE ''%'' + @xDBUserName+ ''%'''
		IF @Pass IS NOT NULL
		SET @sql = @sql + ' AND sm.PassLIKE ''%'' + @xPass+ ''%'''
		IF @UserID IS NOT NULL
		SET @sql = @sql + ' AND sm.UserID= @xUserID'
		IF @CreatedOn IS NOT NULL
		SET @sql = @sql + ' AND sm.CreatedOn= @xCreatedOn'
		IF @DatabaseName IS NOT NULL
		SET @sql = @sql + ' AND sm.DatabaseNameLIKE ''%'' + @xDatabaseName+ ''%'''
		
SET @sql = @sql +
		')
		SELECT *
		FROM Custom_PageSegment
			WHERE
			RowIndex BETWEEN '
			+  CONVERT(nvarchar(10),@CurrentRowIndex)
			+ ' AND (' 
			+ CONVERT(nvarchar(10),@CurrentRowIndex) + ' + ' + CONVERT(nvarchar(10),@PageSize)
			+ ') - 1  order by '
			+ CONVERT(nvarchar(500),@SortOrder);
	SELECT @paramlist = 
		'@xID[uniqueidentifier]
		,@xWebPath[nvarchar](max)
		,@xDatabaseServer[nvarchar](max)
		,@xDBUserName[nvarchar](max)
		,@xPass[nvarchar](max)
		,@xUserID[uniqueidentifier]
		,@xCreatedOn[datetime]
		,@xDatabaseName[nvarchar](max)
		'EXEC sp_executesql @sql, @paramlist, 
			@ID
			,@WebPath
			,@DatabaseServer
			,@DBUserName
			,@Pass
			,@UserID
			,@CreatedOn
			,@DatabaseName
END
GO
/****** Object:  StoredProcedure [dbo].[Project_GetAllProject]    Script Date: 11/14/2014 2:31:04 AM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[Project_GetAllProject]
AS
BEGIN
SET NOCOUNT ON
	SELECT *
	FROM [Project]
END
GO
/****** Object:  StoredProcedure [dbo].[Project_GetProject]    Script Date: 11/14/2014 2:31:04 AM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[Project_GetProject]
	@ID [uniqueidentifier]
AS
BEGIN
SET NOCOUNT ON
	SELECT *
	FROM [dbo].[Project]
	WHERE ID = @ID
END
GO
/****** Object:  StoredProcedure [dbo].[Project_GetProjectByID]    Script Date: 11/14/2014 2:31:04 AM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[Project_GetProjectByID]
	@ID [uniqueidentifier]
AS
BEGIN
SET NOCOUNT ON
	SELECT *
	FROM [dbo].[Project]
	WHERE ID = @ID
END
GO
/****** Object:  StoredProcedure [dbo].[Project_TotalCountProject]    Script Date: 11/14/2014 2:31:04 AM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[Project_TotalCountProject]
	@TotalCount [int] OUTPUT
AS
BEGIN
SET NOCOUNT ON
	SELECT
	@TotalCount = COUNT(*)
	FROM [dbo].[Project]
END
GO
/****** Object:  StoredProcedure [dbo].[Project_TotalCountWithSearchProject]    Script Date: 11/14/2014 2:31:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[Project_TotalCountWithSearchProject]
	@TotalCount [int] OUTPUT,
	@ID [uniqueidentifier] = Null,
	@WebPath [nvarchar](max) = Null,
	@DatabaseServer [nvarchar](max) = Null,
	@DBUserName [nvarchar](max) = Null,
	@Pass [nvarchar](max) = Null,
	@UserID [uniqueidentifier] = Null,
	@CreatedOn [datetime] = Null,
	@DatabaseName [nvarchar](max) = Null
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @sql nvarchar(max),
	@paramlist  nvarchar(4000),
	@xTotalCount int

	SET @sql='SELECT @xTotalCount = COUNT(*)FROM [dbo].[Follow] sm WHERE 1=1'IF @ID IS NOT NULL
		SET @sql = @sql + ' AND sm.ID= @xID'
		IF @WebPath IS NOT NULL
		SET @sql = @sql + ' AND sm.WebPathLIKE ''%'' + @xWebPath+ ''%'''
		IF @DatabaseServer IS NOT NULL
		SET @sql = @sql + ' AND sm.DatabaseServerLIKE ''%'' + @xDatabaseServer+ ''%'''
		IF @DBUserName IS NOT NULL
		SET @sql = @sql + ' AND sm.DBUserNameLIKE ''%'' + @xDBUserName+ ''%'''
		IF @Pass IS NOT NULL
		SET @sql = @sql + ' AND sm.PassLIKE ''%'' + @xPass+ ''%'''
		IF @UserID IS NOT NULL
		SET @sql = @sql + ' AND sm.UserID= @xUserID'
		IF @CreatedOn IS NOT NULL
		SET @sql = @sql + ' AND sm.CreatedOn= @xCreatedOn'
		IF @DatabaseName IS NOT NULL
		SET @sql = @sql + ' AND sm.DatabaseNameLIKE ''%'' + @xDatabaseName+ ''%'''
		SELECT @paramlist = '@xTotalCount [int] OUTPUT, 
		@xID[uniqueidentifier]
		,@xWebPath[nvarchar](max)
		,@xDatabaseServer[nvarchar](max)
		,@xDBUserName[nvarchar](max)
		,@xPass[nvarchar](max)
		,@xUserID[uniqueidentifier]
		,@xCreatedOn[datetime]
		,@xDatabaseName[nvarchar](max)
		'EXEC sp_executesql @sql, @paramlist, @xTotalCount OUTPUT,
			@ID
			,@WebPath
			,@DatabaseServer
			,@DBUserName
			,@Pass
			,@UserID
			,@CreatedOn
			,@DatabaseName
END
GO
/****** Object:  StoredProcedure [dbo].[User_AddUser]    Script Date: 11/14/2014 2:31:04 AM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[User_AddUser]
	@ID [uniqueidentifier],
	@EmailAddress [nvarchar](max),
	@Name [nvarchar](max),
	@Password [nvarchar](max),
	@Salt [nvarchar](max),
	@ImagePath [nvarchar](max),
	@ThambnailPath [nvarchar](max),
	@CreatedOn [datetime]
AS
BEGIN
Declare @IDCount [int];

SELECT @IDCount = COUNT([ID]) FROM [dbo].[User]
WHERE [ID] = @ID

IF @IDCount = 0
	BEGIN
		INSERT INTO [dbo].[User]
			(
				[ID]
				,[EmailAddress]
				,[Name]
				,[Password]
				,[Salt]
				,[ImagePath]
				,[ThambnailPath]
				,[CreatedOn]
				)
			VALUES(@ID
				,@EmailAddress
				,@Name
				,@Password
				,@Salt
				,@ImagePath
				,@ThambnailPath
				,@CreatedOn
			)
	END
END
GO
/****** Object:  StoredProcedure [dbo].[User_DeleteUser]    Script Date: 11/14/2014 2:31:04 AM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[User_DeleteUser]
	@ID [uniqueidentifier]
AS
BEGIN
SET NOCOUNT ON
	DELETE
		FROM [dbo].[User] 
	WHERE ID = @ID
END
GO
/****** Object:  StoredProcedure [dbo].[User_EditUser]    Script Date: 11/14/2014 2:31:04 AM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[User_EditUser]
	@ID [uniqueidentifier],
	@EmailAddress [nvarchar](max),
	@Name [nvarchar](max),
	@Password [nvarchar](max),
	@Salt [nvarchar](max),
	@ImagePath [nvarchar](max),
	@ThambnailPath [nvarchar](max),
	@CreatedOn [datetime]
AS
BEGIN
Declare @IDCount [int];

SELECT @IDCount = COUNT([ID]) FROM [dbo].[User]
WHERE [ID] = @ID

IF @IDCount > 0
	BEGIN
		UPDATE [dbo].[User]
			SET
				[EmailAddress]=@EmailAddress
				,[Name]=@Name
				,[Password]=@Password
				,[Salt]=@Salt
				,[ImagePath]=@ImagePath
				,[ThambnailPath]=@ThambnailPath
				,[CreatedOn]=@CreatedOn
			WHERE [ID]=@ID			
	END
END
GO
/****** Object:  StoredProcedure [dbo].[User_GetAllPagedUser]    Script Date: 11/14/2014 2:31:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[User_GetAllPagedUser]
	@CurrentPageIndex [int],
	@PageSize [int],
	@SortOrder [nvarchar](500),
	@ID [uniqueidentifier] = Null,
	@EmailAddress [nvarchar](max) = Null,
	@Name [nvarchar](max) = Null,
	@Password [nvarchar](max) = Null,
	@Salt [nvarchar](max) = Null,
	@ImagePath [nvarchar](max) = Null,
	@ThambnailPath [nvarchar](max) = Null,
	@CreatedOn [datetime] = Null
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @sql nvarchar(max),
	@paramlist  nvarchar(4000),
	@CurrentRowIndex int;

	SET @CurrentRowIndex = ((@CurrentPageIndex - 1) * @PageSize) + 1;

	SET @sql='
		WITH Custom_PageSegment AS (
			SELECT *,
			ROW_NUMBER() OVER ( ORDER BY ' + @SortOrder + ' ) as RowIndex
			FROM [dbo].[User] sm
			WHERE 1=1'

		IF @ID IS NOT NULL
		SET @sql = @sql + ' AND sm.ID= @xID'
		IF @EmailAddress IS NOT NULL
		SET @sql = @sql + ' AND sm.EmailAddressLIKE ''%'' + @xEmailAddress+ ''%'''
		IF @Name IS NOT NULL
		SET @sql = @sql + ' AND sm.NameLIKE ''%'' + @xName+ ''%'''
		IF @Password IS NOT NULL
		SET @sql = @sql + ' AND sm.PasswordLIKE ''%'' + @xPassword+ ''%'''
		IF @Salt IS NOT NULL
		SET @sql = @sql + ' AND sm.SaltLIKE ''%'' + @xSalt+ ''%'''
		IF @ImagePath IS NOT NULL
		SET @sql = @sql + ' AND sm.ImagePathLIKE ''%'' + @xImagePath+ ''%'''
		IF @ThambnailPath IS NOT NULL
		SET @sql = @sql + ' AND sm.ThambnailPathLIKE ''%'' + @xThambnailPath+ ''%'''
		IF @CreatedOn IS NOT NULL
		SET @sql = @sql + ' AND sm.CreatedOn= @xCreatedOn'
		
SET @sql = @sql +
		')
		SELECT *
		FROM Custom_PageSegment
			WHERE
			RowIndex BETWEEN '
			+  CONVERT(nvarchar(10),@CurrentRowIndex)
			+ ' AND (' 
			+ CONVERT(nvarchar(10),@CurrentRowIndex) + ' + ' + CONVERT(nvarchar(10),@PageSize)
			+ ') - 1  order by '
			+ CONVERT(nvarchar(500),@SortOrder);
	SELECT @paramlist = 
		'@xID[uniqueidentifier]
		,@xEmailAddress[nvarchar](max)
		,@xName[nvarchar](max)
		,@xPassword[nvarchar](max)
		,@xSalt[nvarchar](max)
		,@xImagePath[nvarchar](max)
		,@xThambnailPath[nvarchar](max)
		,@xCreatedOn[datetime]
		'EXEC sp_executesql @sql, @paramlist, 
			@ID
			,@EmailAddress
			,@Name
			,@Password
			,@Salt
			,@ImagePath
			,@ThambnailPath
			,@CreatedOn
END
GO
/****** Object:  StoredProcedure [dbo].[User_GetAllUser]    Script Date: 11/14/2014 2:31:04 AM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[User_GetAllUser]
AS
BEGIN
SET NOCOUNT ON
	SELECT *
	FROM [User]
END
GO
/****** Object:  StoredProcedure [dbo].[User_GetUser]    Script Date: 11/14/2014 2:31:04 AM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[User_GetUser]
	@ID [uniqueidentifier]
AS
BEGIN
SET NOCOUNT ON
	SELECT *
	FROM [dbo].[User]
	WHERE ID = @ID
END
GO
/****** Object:  StoredProcedure [dbo].[User_GetUserByID]    Script Date: 11/14/2014 2:31:04 AM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[User_GetUserByID]
	@ID [uniqueidentifier]
AS
BEGIN
SET NOCOUNT ON
	SELECT *
	FROM [dbo].[User]
	WHERE ID = @ID
END
GO
/****** Object:  StoredProcedure [dbo].[User_TotalCountUser]    Script Date: 11/14/2014 2:31:04 AM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[User_TotalCountUser]
	@TotalCount [int] OUTPUT
AS
BEGIN
SET NOCOUNT ON
	SELECT
	@TotalCount = COUNT(*)
	FROM [dbo].[User]
END
GO
/****** Object:  StoredProcedure [dbo].[User_TotalCountWithSearchUser]    Script Date: 11/14/2014 2:31:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[User_TotalCountWithSearchUser]
	@TotalCount [int] OUTPUT,
	@ID [uniqueidentifier] = Null,
	@EmailAddress [nvarchar](max) = Null,
	@Name [nvarchar](max) = Null,
	@Password [nvarchar](max) = Null,
	@Salt [nvarchar](max) = Null,
	@ImagePath [nvarchar](max) = Null,
	@ThambnailPath [nvarchar](max) = Null,
	@CreatedOn [datetime] = Null
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @sql nvarchar(max),
	@paramlist  nvarchar(4000),
	@xTotalCount int

	SET @sql='SELECT @xTotalCount = COUNT(*)FROM [dbo].[Follow] sm WHERE 1=1'IF @ID IS NOT NULL
		SET @sql = @sql + ' AND sm.ID= @xID'
		IF @EmailAddress IS NOT NULL
		SET @sql = @sql + ' AND sm.EmailAddressLIKE ''%'' + @xEmailAddress+ ''%'''
		IF @Name IS NOT NULL
		SET @sql = @sql + ' AND sm.NameLIKE ''%'' + @xName+ ''%'''
		IF @Password IS NOT NULL
		SET @sql = @sql + ' AND sm.PasswordLIKE ''%'' + @xPassword+ ''%'''
		IF @Salt IS NOT NULL
		SET @sql = @sql + ' AND sm.SaltLIKE ''%'' + @xSalt+ ''%'''
		IF @ImagePath IS NOT NULL
		SET @sql = @sql + ' AND sm.ImagePathLIKE ''%'' + @xImagePath+ ''%'''
		IF @ThambnailPath IS NOT NULL
		SET @sql = @sql + ' AND sm.ThambnailPathLIKE ''%'' + @xThambnailPath+ ''%'''
		IF @CreatedOn IS NOT NULL
		SET @sql = @sql + ' AND sm.CreatedOn= @xCreatedOn'
		SELECT @paramlist = '@xTotalCount [int] OUTPUT, 
		@xID[uniqueidentifier]
		,@xEmailAddress[nvarchar](max)
		,@xName[nvarchar](max)
		,@xPassword[nvarchar](max)
		,@xSalt[nvarchar](max)
		,@xImagePath[nvarchar](max)
		,@xThambnailPath[nvarchar](max)
		,@xCreatedOn[datetime]
		'EXEC sp_executesql @sql, @paramlist, @xTotalCount OUTPUT,
			@ID
			,@EmailAddress
			,@Name
			,@Password
			,@Salt
			,@ImagePath
			,@ThambnailPath
			,@CreatedOn
END
GO
/****** Object:  Table [dbo].[Module]    Script Date: 11/14/2014 2:31:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Module](
	[ID] [uniqueidentifier] NOT NULL,
	[ModulePath] [nvarchar](max) NOT NULL,
	[Namespace] [nvarchar](max) NOT NULL,
	[ProjectID] [uniqueidentifier] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_Module] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Project]    Script Date: 11/14/2014 2:31:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Project](
	[ID] [uniqueidentifier] NOT NULL,
	[WebPath] [nvarchar](max) NOT NULL,
	[DatabaseServer] [nvarchar](max) NOT NULL,
	[DBUserName] [nvarchar](max) NOT NULL,
	[Pass] [nvarchar](max) NOT NULL,
	[UserID] [uniqueidentifier] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[DatabaseName] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_Project] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[User]    Script Date: 11/14/2014 2:31:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[ID] [uniqueidentifier] NOT NULL,
	[EmailAddress] [nvarchar](max) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Password] [nvarchar](max) NOT NULL,
	[Salt] [nvarchar](max) NOT NULL,
	[ImagePath] [nvarchar](max) NULL,
	[ThambnailPath] [nvarchar](max) NULL,
	[CreatedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER TABLE [dbo].[Module]  WITH CHECK ADD  CONSTRAINT [FK_Module_Project] FOREIGN KEY([ProjectID])
REFERENCES [dbo].[Project] ([ID])
GO
ALTER TABLE [dbo].[Module] CHECK CONSTRAINT [FK_Module_Project]
GO
ALTER TABLE [dbo].[Project]  WITH CHECK ADD  CONSTRAINT [FK_Project_User] FOREIGN KEY([UserID])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[Project] CHECK CONSTRAINT [FK_Project_User]
GO
