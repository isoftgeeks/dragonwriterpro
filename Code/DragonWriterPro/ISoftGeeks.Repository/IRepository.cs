﻿using ISoftGeeks.Enums;
using System;
using System.Collections.Generic;

namespace ISoftGeeks.Data
{
    public interface IRepository<T>
    {
        Status AddItem(T item);
        Status UpdateItem(T item);
        Status DeleteItem(Guid id);
        T GetItem(Guid id);
        IEnumerable<T> GetAllItem();
        IEnumerable<T> GetPagedAllItem(int pageSize, int pageNumber);
        IEnumerable<T> GetPagedAllItem(int pageSize, int pageNumber, T searchItem, string sortOrder);
        int GetTotalCount();
        int GetTotalCount(T searchItem, string sirtOrder);
        IEnumerable<X> ExecuteStoreProcidure<X>(string procidureName, object parameters);
        T ExecuteScalerStoreProcidure(string procidureName, object parameters);
        Status ExecuteNonQueryStoreProcidure<X>(X item, string SPname);
    }
}
