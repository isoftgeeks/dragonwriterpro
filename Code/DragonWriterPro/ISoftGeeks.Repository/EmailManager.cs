﻿using ISoftGeeks.Utility;
using RazorEngine.Templating;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISoftGeeks.Data
{
    public class EmailManager
    {
        private void Send(List<NameWithEmail> receivers, NameWithEmail sender, string subject, string body)
        {
            try
            {
                // Assigning mail settings from appsetting
                EmailSettings settings = new EmailSettings();
                settings.Host = "mail.isoftgeeks.com";
                settings.Username = "noreply@isoftgeeks.com";
                settings.Password = "noreply@123";
                settings.Port = 2525;
                settings.UseSSL = false;

                // sending registration email
                EmailSender emailSender = new EmailSender(settings);
                emailSender.Subject = subject;
                emailSender.Body = body;
                emailSender.Sender = sender;
                emailSender.ReplyToList = new List<NameWithEmail>() { sender };
                emailSender.Receivers = receivers;
                emailSender.Priority = System.Net.Mail.MailPriority.High;
                emailSender.EmailType = EmailTypeOptions.HtmlMail;
                emailSender.SendMail();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void SendErrorMail(string errorMessage)
        {
            ErrorTempleteModel model = new ErrorTempleteModel();
            model.ErrorBody = errorMessage;
            var templateFilePath = "/EmailTemplates/Error.cshtml";
            var templateService = new TemplateService();
            var emailHtmlBody = templateService.Parse(File.ReadAllText(templateFilePath), model, null, null);

            List<NameWithEmail> receivers = new List<NameWithEmail>();

            receivers.Add(new NameWithEmail() { Email = "ahsan@isoftgeeks.com", Name = "AhsaN" });

            NameWithEmail sender = new NameWithEmail() { Name = "ISoftGeeks", Email = "noreply@isoftgeeks.com" };

            Send(receivers, sender, "DataAccess Error", emailHtmlBody);
        }
    }
}
