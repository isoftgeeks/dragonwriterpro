﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using System.Transactions;

namespace ISoftGeeks.Data
{
    public class DataAccess
    {
        private SqlConnection sqlConnection;
        private SqlCommand command;
        private string dbConnectionString;

        public DataAccess(string connectionString)
        {
            dbConnectionString = connectionString;
        }

        private void DBConnection(string procidureName)
        {
            command = new SqlCommand(procidureName, sqlConnection);
            command.CommandType = CommandType.StoredProcedure;
        }

        private void AddParameter(Dictionary<string, string> parameters, SqlCommand command, string DataType)
        {
            if (parameters != null)
            {
                foreach (var parameter in parameters)
                {
                    var name = parameter.Key;
                    var value = (string.IsNullOrEmpty(parameter.Value)) ? (object)DBNull.Value : parameter.Value;
                    //if (value == "")
                    //{
                    //    value  =  DBNull.Value.ToString() ;
                    //}

                    switch (DataType)
                    {
                        case "System.String":
                            command.Parameters.Add("@" + name, SqlDbType.NVarChar).Value = value;
                            break;
                        case "System.Guid":
                            command.Parameters.Add("@" + name, SqlDbType.UniqueIdentifier).Value = (value != DBNull.Value) ? new Guid(value.ToString()) : value;
                            break;
                        case "System.DateTime":
                            command.Parameters.Add("@" + name, SqlDbType.DateTime).Value = (value != DBNull.Value) ? Convert.ToDateTime(value) : value;
                            break;
                        case "System.Int32":
                        case "System.Int64":
                            command.Parameters.Add("@" + name, SqlDbType.Int).Value = value;
                            break;
                        case "System.Double":
                            command.Parameters.Add("@" + name, SqlDbType.Float).Value = value;
                            break;
                        case "System.Boolean":
                            command.Parameters.Add("@" + name, SqlDbType.Bit).Value = value;
                            break;
                        default:
                            break;
                    }

                }
            }
        }

        public int ExecuteNonQuery<T>(T obj, string procidureName)
        {

            using (sqlConnection = new SqlConnection(dbConnectionString))
            {
                try
                {
                    DBConnection(procidureName);

                    Type type = obj.GetType();
                    PropertyInfo[] properties = type.GetProperties();
                    foreach (PropertyInfo propertyInfo in properties)
                    {
                        Dictionary<string, string> parameters = new Dictionary<string, string>();
                        var name = propertyInfo.Name;
                        var value = propertyInfo.GetValue(obj, null);
                        if (value != null)
                        {
                            parameters.Add(name, value.ToString());
                        }
                        else
                        {
                            parameters.Add(name, "");
                        }
                        AddParameter(parameters, command, propertyInfo.PropertyType.FullName);
                    }

                    sqlConnection.Open();
                    command.ExecuteNonQuery();
                    sqlConnection.Close();
                    return 1;
                }
                catch (SqlException ex)
                {
                    EmailManager manager = new EmailManager();
                    manager.SendErrorMail(JsonConvert.SerializeObject(obj) + "\n\n" + ex.InnerException + "\n\n" + ex.Message);
                    return 0;
                }
            }
        }

        public int DeleteItem(Guid id, string procidureName)
        {
            try
            {
                using (sqlConnection = new SqlConnection(dbConnectionString))
                {
                    DBConnection(procidureName);
                    Dictionary<string, string> parameters = new Dictionary<string, string>();
                    var name = "ID";
                    var value = id;
                    parameters.Add(name, value.ToString());

                    AddParameter(parameters, command, "System.Guid");

                    sqlConnection.Open();
                    command.ExecuteNonQuery();
                    sqlConnection.Close();
                    return 1;
                }
            }
            catch (SqlException ex)
            {
                throw;
            }
        }

        public IEnumerable<T> ExecuteReader<T>(object obj, string procidureName, object searchObject)
        {
            List<T> result = new List<T>();

            Type classType = typeof(T);            
            SqlDataReader dataReader;
            try
            {
                var transactionOptions = new TransactionOptions();
                transactionOptions.Timeout = new TimeSpan(0, 0, 5, 0);
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, transactionOptions))
                {
                    using (sqlConnection = new SqlConnection(dbConnectionString))
                    {
                        DBConnection(procidureName);
                        if (obj != null)
                        {
                            Type type = obj.GetType();
                            if (type.Name == "Guid")
                            {
                                Dictionary<string, string> parameters = new Dictionary<string, string>();
                                parameters.Add("ID", obj.ToString());
                                AddParameter(parameters, command, type.FullName);
                            }
                            else
                            {
                                PropertyInfo[] properties = type.GetProperties();
                                foreach (PropertyInfo propertyInfo in properties)
                                {
                                    Dictionary<string, string> parameters = new Dictionary<string, string>();
                                    var name = propertyInfo.Name;
                                    var value = propertyInfo.GetValue(obj, null);
                                    if (value != null && value.ToString() != Guid.Empty.ToString() && name != "CreatedOn")
                                    {
                                        parameters.Add(name, value.ToString());
                                    }
                                    else
                                    {
                                        parameters.Add(name, "");
                                    }
                                    AddParameter(parameters, command, propertyInfo.PropertyType.FullName);
                                }
                            }
                        }
                        if (searchObject != null)
                        {
                            Type type = searchObject.GetType();
                            PropertyInfo[] properties = type.GetProperties();
                            foreach (PropertyInfo propertyInfo in properties)
                            {
                                Dictionary<string, string> parameters = new Dictionary<string, string>();
                                var name = propertyInfo.Name;
                                var value = propertyInfo.GetValue(searchObject, null);
                                if (value != null && value.ToString() != Guid.Empty.ToString() && name != "CreatedOn")
                                {
                                    parameters.Add(name, value.ToString());
                                }
                                else
                                {
                                    parameters.Add(name, "");
                                }
                                AddParameter(parameters, command, propertyInfo.PropertyType.FullName);
                            }
                        }


                        //object parameters = new { PageSize = pageSize, PageNumber = pageNumber, SortOrder = "ASC" };
                        sqlConnection.Open();
                        dataReader = command.ExecuteReader();

                        while (dataReader.Read())
                        {
                            T item = (T)Activator.CreateInstance(classType);
                            PropertyInfo[] propertyInfos = typeof(T).GetProperties();
                            foreach (var propertyInfo in propertyInfos)
                            {
                                var data = (dataReader[propertyInfo.Name] == DBNull.Value)
                                    ? string.Empty
                                    : dataReader[propertyInfo.Name];
                                propertyInfo.SetValue(item, data);
                            }
                            result.Add(item);
                        }
                    }
                }
            }
            catch
                (Exception ex)
            {
                throw;
            }

            return result;
        }

        public int GetTotalCount(string procidureName)
        {
            try
            {
                using (sqlConnection = new SqlConnection(dbConnectionString))
                {
                    DBConnection(procidureName);
                    SqlParameter parameter = new SqlParameter("@TotalCount", SqlDbType.Int);
                    command.Parameters.Add(parameter).Direction = ParameterDirection.Output;

                    sqlConnection.Open();
                    command.ExecuteNonQuery();
                    int totalCount = Convert.ToInt32(command.Parameters["@TotalCount"].Value);
                    sqlConnection.Close();

                    return totalCount;
                }
            }
            catch (SqlException ex)
            {
                throw;
            }
        }

        public int GetTotalCount<T>(string procidureName, T obj)
        {
            try
            {
                using (sqlConnection = new SqlConnection(dbConnectionString))
                {
                    DBConnection(procidureName);
                    SqlParameter parameter = new SqlParameter("@TotalCount", SqlDbType.Int);
                    command.Parameters.Add(parameter).Direction = ParameterDirection.Output;

                    Type type = obj.GetType();
                    PropertyInfo[] properties = type.GetProperties();                    
                    foreach (PropertyInfo propertyInfo in properties)
                    {
                        Dictionary<string, string> parameters = new Dictionary<string, string>();
                        var name = propertyInfo.Name;
                        var value = propertyInfo.GetValue(obj, null);
                        if (value != null && value.ToString() != Guid.Empty.ToString() && name != "CreatedOn")
                        {
                            parameters.Add(name, value.ToString());
                        }
                        else
                        {
                            parameters.Add(name, "");
                        }
                        AddParameter(parameters, command, propertyInfo.PropertyType.FullName);
                    }

                    sqlConnection.Open();
                    command.ExecuteNonQuery();
                    int totalCount = Convert.ToInt32(command.Parameters["@TotalCount"].Value);
                    sqlConnection.Close();

                    return totalCount;
                }
            }
            catch (SqlException ex)
            {
                throw;
            }
        }
    }
}
