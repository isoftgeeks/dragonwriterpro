﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using ISoftGeeks.Enums;

namespace ISoftGeeks.Data
{
    public class Repository<T> : IRepository<T>
    {
        private DataAccess da;
        private string _className = typeof(T).Name;

        public Repository(String connectionString)
        {
            da = new DataAccess(connectionString);
        }

        public Status AddItem(T item)
        {
            try
            {
                da.ExecuteNonQuery<T>(item, _className + "_Add" + _className);
                return Status.Success;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public Status UpdateItem(T item)
        {
            try
            {
                da.ExecuteNonQuery<T>(item, _className + "_Edit" + _className);
                return Status.Success;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public Status DeleteItem(Guid id)
        {
            try
            {
                da.DeleteItem(id, _className + "_Delete" + _className);
                return Status.Success;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public T GetItem(Guid id)
        {
            try
            {
                return da.ExecuteReader<T>(id, _className + "_Get" + _className + "ByID", null).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public IEnumerable<T> GetAllItem()
        {
            try
            {
                return da.ExecuteReader<T>(null, _className + "_GetAll" + _className, null);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public IEnumerable<T> GetPagedAllItem(int pageSize, int pageNumber)
        {
            object parameters = new { PageSize = pageSize, PageNumber = pageNumber, SortOrder = "CreatedOn DESC" };
            try
            {
                return da.ExecuteReader<T>(null, _className + "_GetAllPaged" + _className, parameters);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public IEnumerable<T> GetPagedAllItem(int pageSize, int pageNumber, T searchItem, string sortOrder)
        {
            object parameters;

            if (sortOrder == null)
            {

                parameters = new { PageSize = pageSize, PageNumber = pageNumber, SortOrder = "ID ASC" };
            }
            else
            {                
                parameters = new { PageSize = pageSize, PageNumber = pageNumber, SortOrder = sortOrder };
            }

            try
            {
                return da.ExecuteReader<T>(searchItem, _className + "_GetAllPaged" + _className, parameters);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public int GetTotalCount(T searchItem, string sortOrder)
        {
            return da.GetTotalCount(_className + "_TotalCountWithSearch" + _className, searchItem);
        }

        public int GetTotalCount()
        {
            return da.GetTotalCount(_className + "_TotalCount" + _className);
        }

        public IEnumerable<X> ExecuteStoreProcidure<X>(string procidureName, object parameters)
        {
            return da.ExecuteReader<X>(parameters, procidureName,null);
        }

        public T ExecuteScalerStoreProcidure(string procidureName, object parameters)
        {
            return da.ExecuteReader<T>(parameters, procidureName,null).FirstOrDefault();
        }

        public Status ExecuteNonQueryStoreProcidure<X>(X item, string SPname)
        {
            try
            {
                da.ExecuteNonQuery<X>(item, SPname);
                return Status.Success;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

    }
}
