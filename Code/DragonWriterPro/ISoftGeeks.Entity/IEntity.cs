﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISoftGeeks.Entity
{
    public interface IEntity
    {
        Guid ID { get; set; }
    }
}
