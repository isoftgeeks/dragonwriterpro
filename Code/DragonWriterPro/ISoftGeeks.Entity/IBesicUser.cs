﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISoftGeeks.Entity
{
    public interface IBesicUser : IEntity
    {
        string UserName { get; set; }
        string Email { get; set; }
        string Phone { get; set; }
        string Password { get; set; }
        string Salt { get; set; }
    }
}
