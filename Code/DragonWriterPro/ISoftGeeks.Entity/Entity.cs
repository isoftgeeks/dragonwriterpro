﻿using System;

namespace ISoftGeeks.Entity
{
    public class Entity:IEntity
    {
        public Guid ID { get; set; }
    }
}
