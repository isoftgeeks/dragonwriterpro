﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ISoftGeeks.IO
{
    public class AmazonRawImageInfo
    {
        public Guid ParentID { get; set; }
        public string TempPath { get; set; }
        public string DestPath { get; set; }
        public string ThambPath { get; set; }

        public List<ImageSize> ImageSizes { get; set; }

        public string ImageType { get; set; }
    }
}