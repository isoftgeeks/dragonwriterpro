﻿using Amazon.S3.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISoftGeeks.IO
{
    public class ImageHelperForAmazon : ImageHelper
    {
        public static void UploadImageToAmazon(ImageSettingsForAmazon imageSettings)
        {
            try
            {
                var client = AmazonSettings.GetS3Client();
                PutObjectRequest request = new PutObjectRequest();
                request.BucketName = "isoftgeeks";
                request.Key = imageSettings.TempPath + imageSettings.ImgFile.FileName;
                request.InputStream = imageSettings.ImgFile.InputStream;
                client.PutObject(request);
                AmazonSQSManager.SendRawImageInfoToAmazonSQS(imageSettings);                
            }
            catch (Exception)
            {
                throw;
            }

        }

        
    }
}
