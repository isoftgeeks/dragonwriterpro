﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISoftGeeks.IO
{
    public class ImageSettingsForAmazon:ImageSettings
    {
        public Guid ParentID { get; set; }
        public string ThambPath { get; set; }
        public string DestPath { get; set; }
        public string ImageType { get; set; }
    }
}
