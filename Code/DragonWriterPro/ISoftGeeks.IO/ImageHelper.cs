﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Web;

namespace ISoftGeeks.IO
{
    public class ImageHelper
    {
        public static List<string> UploadImage(ImageSettings imageSettings)
        {
            var pathList = new List<string>();

            try
            {
                if (imageSettings.ImgFile != null)
                {
                    string ext = Path.GetExtension(imageSettings.ImgFile.FileName).ToLower();
                    if (new[] { ".jpg", ".png", ".bmp" }.Contains(ext))
                    {
                        if (imageSettings.ImgFile.ContentLength > 0 && imageSettings.ImgFile.ContentLength < imageSettings.MaxImageSize)
                        {
                            string filePath = HttpContext.Current.Server.MapPath(Path.Combine(imageSettings.TempPath + imageSettings.ImgFile.FileName));
                            imageSettings.ImgFile.SaveAs(filePath);
                            foreach (var imageSize in imageSettings.ImageSizes)
                            {
                                var newImage = ResizeImage(imageSize, imageSettings.TempPath, imageSettings.ImgFile.FileName, imageSettings.ImgFile);
                                var fileName = Guid.NewGuid() + "_" + imageSize.Width + "W_" + imageSize.Height + "H" + ext;
                                filePath = HttpContext.Current.Server.MapPath(Path.Combine(imageSettings.DestinationPath + "\\" + fileName));
                                pathList.Add(Path.Combine(imageSettings.DestinationPath + "/" + fileName));
                                newImage.Save(filePath, ImageFormat.Jpeg);
                            }
                            if (File.Exists(filePath))
                            {
                                File.Delete(filePath);
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
            return pathList;
        }

        private static System.Drawing.Image ResizeImage(ImageSize imageSize, string tempPath, string filename, HttpPostedFileBase image)
        {
            try
            {
                var oldImage = System.Drawing.Image.FromFile(HttpContext.Current.Server.MapPath(Path.Combine(tempPath + filename)));

                #region Aspect Retio
                double aspectRetio = 0.0;
                if (oldImage.Height > oldImage.Width)
                {
                    aspectRetio = (double)(oldImage.Height) / oldImage.Width;
                }
                else
                {
                    aspectRetio = (double)oldImage.Width / oldImage.Height;
                }
                #endregion


                if (imageSize.Height == 0.0 && imageSize.Width > 0 && oldImage.Width > imageSize.Width)
                {
                    imageSize.Height = oldImage.Height / aspectRetio;
                }
                else if (imageSize.Height == 0.0 && imageSize.Width > 0 && oldImage.Width < imageSize.Width)
                {
                    imageSize.Width = oldImage.Width;
                    imageSize.Height = oldImage.Height;
                }
                else if (imageSize.Height > 0 && imageSize.Width == 0.0 && oldImage.Height > imageSize.Height)
                {
                    imageSize.Width = imageSize.Height * aspectRetio;
                }
                else if (imageSize.Height > 0 && imageSize.Width == 0.0 && oldImage.Height < imageSize.Height)
                {
                    imageSize.Width = oldImage.Width;
                    imageSize.Height = oldImage.Height;
                }

                var newImage = new Bitmap((int)imageSize.Width, (int)imageSize.Height);
                var file = new FileInfo(HttpContext.Current.Server.MapPath(Path.Combine(tempPath + filename)));
                if (file.Exists)
                {

                    using (Graphics gr = Graphics.FromImage(newImage))
                    {
                        gr.SmoothingMode = SmoothingMode.HighQuality;
                        gr.InterpolationMode = InterpolationMode.HighQualityBicubic;
                        gr.PixelOffsetMode = PixelOffsetMode.HighQuality;
                        if (imageSize.Height > 0 && imageSize.Width > 0)
                        {
                            gr.DrawImage(System.Drawing.Image.FromStream(image.InputStream, true, true), new Rectangle(0, 0, (int)imageSize.Width, (int)imageSize.Height));
                        }

                    }
                }
                return newImage;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
    
    }
}
