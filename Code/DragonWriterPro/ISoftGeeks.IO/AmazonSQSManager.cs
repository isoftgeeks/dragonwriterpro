﻿using Amazon;
using Amazon.SQS;
using Amazon.SQS.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISoftGeeks.IO
{
    public static class AmazonSQSManager
    {
        public static void SendRawImageInfoToAmazonSQS(ImageSettingsForAmazon imageSettings)
        {
            try
            {
                AmazonRawImageInfo info = new AmazonRawImageInfo();
                info.ParentID = imageSettings.ParentID;
                info.TempPath = imageSettings.TempPath + imageSettings.ImgFile.FileName;
                info.ImageSizes = imageSettings.ImageSizes;
                info.ThambPath = imageSettings.ThambPath;
                info.DestPath = imageSettings.DestPath;
                info.ImageType = imageSettings.ImageType;

                IAmazonSQS sqs = AWSClientFactory.CreateAmazonSQSClient(RegionEndpoint.APSoutheast1);
                SendMessageRequest sendMessageRequest = new SendMessageRequest();
                sendMessageRequest.QueueUrl = "https://sqs.ap-southeast-1.amazonaws.com/764831637036/ImageResizeQueue"; //URL from initial queue creation
                sendMessageRequest.MessageBody = JsonConvert.SerializeObject(info);
                sqs.SendMessage(sendMessageRequest);
            }
            catch (Exception)
            {

                throw;
            }

        }
    }
}
