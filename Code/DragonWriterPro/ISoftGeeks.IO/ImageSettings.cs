﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ISoftGeeks.IO
{
    public class ImageSettings
    {
        public HttpPostedFileBase ImgFile { get; set; }
        public string DestinationPath { get; set; }
        public string TempPath { get; set; }
        public int MaxImageSize { get; set; }
        public List<ImageSize> ImageSizes { get; set; }
    }
}
