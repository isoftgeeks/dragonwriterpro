﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using Amazon;
using Amazon.S3;
using System.Configuration;

namespace ISoftGeeks.IO
{
    public static class AmazonSettings
    {
        public static IAmazonS3 GetS3Client()
        {
            NameValueCollection appConfig = ConfigurationManager.AppSettings;

            IAmazonS3 s3Client = AWSClientFactory.CreateAmazonS3Client(
                    appConfig["AWSAccessKey"],
                    appConfig["AWSSecretKey"],
                    RegionEndpoint.APSoutheast1
                    );
            return s3Client;
        }
    }
}