﻿using Ninject.Modules;
using System.Configuration;
namespace DragonWriter
{
    public class DragonWriterModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IEntity>().To<Entity>();
            Bind<IModule>().To<Module>();
            Bind<IProject>().To<Project>();

            Bind<IDragonWriterService>().To<DragonWriterService>();

            Bind<IModuleRepository<Module>>().To<ModuleRepository<Module>>().WithConstructorArgument("dbConnection", ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            Bind<IProjectRepository<Project>>().To<ProjectRepository<Project>>().WithConstructorArgument("dbConnection", ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
        }
    }
}
