using System;
namespace DragonWriter
{
	public class Project : Entity, IProject
	{
		public string WebPath { get; set; }
		public string DatabaseServer { get; set; }
		public string DBUserName { get; set; }
		public string Pass { get; set; }
		public Guid UserID { get; set; }
		public string DatabaseName { get; set; }
		
	}
}