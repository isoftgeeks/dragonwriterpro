using System;
namespace DragonWriter
{
	public class Module : Entity, IModule
	{
		public string ModulePath { get; set; }
		public string Namespace { get; set; }
		public Guid ProjectID { get; set; }
		
	}
}