using System;
namespace DragonWriter
{
	 public interface IEntity
	{
		Guid ID { get; set; }
		DateTime CreatedOn { get; set; }
	}
}