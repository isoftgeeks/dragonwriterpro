using System;
namespace DragonWriter
{
	public interface IProject : IEntity
	{
		string WebPath { get; set; }
		string DatabaseServer { get; set; }
		string DBUserName { get; set; }
		string Pass { get; set; }
		Guid UserID { get; set; }
		string DatabaseName { get; set; }
		
	}
}