using System;
namespace DragonWriter
{
	public interface IModule : IEntity
	{
		string ModulePath { get; set; }
		string Namespace { get; set; }
		Guid ProjectID { get; set; }
		
	}
}