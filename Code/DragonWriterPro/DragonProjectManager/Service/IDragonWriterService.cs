using System;
using System.Collections.Generic;

namespace DragonWriter
{
	public interface IDragonWriterService
	{
		#region Module
		void AddModule(Module module);
		void EditModule(Module module);
		void DeleteModule(Guid id);
		Module GetModule(Guid id);
		IEnumerable<Module> GetAllModule();
		IEnumerable<Module> GetAllPagedModule(int pageSize, int pageNumber);
		int GetTotalModule();
        IEnumerable<Module> GetAllPagedModuleWithSearch(int pageSize, int pageNumber, Module module, string sortOrder);
		int GetTotalModuleWithSearch(Module module, string sortOrder);
		#endregion

		#region Project
		void AddProject(Project project);
		void EditProject(Project project);
		void DeleteProject(Guid id);
		Project GetProject(Guid id);
		IEnumerable<Project> GetAllProject();
		IEnumerable<Project> GetAllPagedProject(int pageSize, int pageNumber);
		int GetTotalProject();
        IEnumerable<Project> GetAllPagedProjectWithSearch(int pageSize, int pageNumber, Project project, string sortOrder);
		int GetTotalProjectWithSearch(Project project, string sortOrder);
		#endregion

		}
}