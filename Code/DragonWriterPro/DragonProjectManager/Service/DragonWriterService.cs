using System;
using System.Collections.Generic;
using ISoftGeeks.Utility;
using Ninject;

namespace DragonWriter
{
    public class DragonWriterService : IDragonWriterService
    {
        private Logger _logger;
        IModuleRepository<Module> _moduleRepository;
        IProjectRepository<Project> _projectRepository;

        [Inject]
        public DragonWriterService(IModuleRepository<Module> moduleRepository, IProjectRepository<Project> projectRepository)
        {
            _moduleRepository = moduleRepository;
            _projectRepository = projectRepository;
            _logger = new Logger();
        }

        #region Module
        public void AddModule(Module module)
        {
            try
            {
                _moduleRepository.AddItem(module);
            }
            catch (Exception exception)
            {
                _logger.WriteLog("Cannot insert data \n-----------------\n" + exception);
                throw;
            }
        }

        public void EditModule(Module module)
        {
            try
            {
                _moduleRepository.UpdateItem(module);
            }
            catch (Exception exception)
            {
                _logger.WriteLog("Cannot Edit data \n-----------------\n" + exception);
                throw;
            }
        }

        public void DeleteModule(Guid id)
        {
            try
            {
                _moduleRepository.DeleteItem(id);
            }
            catch (Exception exception)
            {
                _logger.WriteLog("Cannot Delete data \n-----------------\n" + exception);
                throw;
            }
        }

        public IEnumerable<Module> GetAllModule()
        {
            try
            {
                return _moduleRepository.GetAllItem();
            }
            catch (Exception exception)
            {
                _logger.WriteLog("Cannot GetAll data \n-----------------\n" + exception);
                throw;
            }
        }

        public IEnumerable<Module> GetAllPagedModule(int pageSize, int pageNumber)
        {
            try
            {
                return _moduleRepository.GetPagedAllItem(pageSize, pageNumber);
            }
            catch (Exception exception)
            {
                _logger.WriteLog("Cannot GetAllPaged data \n-----------------\n" + exception);
                throw;
            }
        }

        public Module GetModule(Guid id)
        {
            try
            {
                return _moduleRepository.GetItem(id);
            }
            catch (Exception exception)
            {
                _logger.WriteLog("Cannot Get data \n-----------------\n" + exception);
                throw;
            }
        }

        public int GetTotalModule()
        {
            try
            {
                return _moduleRepository.GetTotalCount();
            }
            catch (Exception exception)
            {
                _logger.WriteLog("Cannot GetTotal data \n-----------------\n" + exception);
                throw;
            }
        }

        public IEnumerable<Module> GetAllPagedModuleWithSearch(int pageSize, int pageNumber, Module module, string sortOrder)
        {
            try
            {
                return _moduleRepository.GetPagedAllItem(pageSize, pageNumber, module, sortOrder);
            }
            catch (Exception exception)
            {
                _logger.WriteLog("Cannot GetAllPaged data \n-----------------\n" + exception);
                throw;
            }
        }

        public int GetTotalModuleWithSearch(Module module, string sortOrder)
        {
            try
            {
                return _moduleRepository.GetTotalCount(module, sortOrder);
            }
            catch (Exception exception)
            {
                _logger.WriteLog("Cannot GetTotal data WithSearch \n-----------------\n" + exception);
                throw;
            }
        }

        #endregion

        #region Project
        public void AddProject(Project project)
        {
            try
            {
                _projectRepository.AddItem(project);
            }
            catch (Exception exception)
            {
                _logger.WriteLog("Cannot insert data \n-----------------\n" + exception);
                throw;
            }
        }

        public void EditProject(Project project)
        {
            try
            {
                _projectRepository.UpdateItem(project);
            }
            catch (Exception exception)
            {
                _logger.WriteLog("Cannot Edit data \n-----------------\n" + exception);
                throw;
            }
        }

        public void DeleteProject(Guid id)
        {
            try
            {
                _projectRepository.DeleteItem(id);
            }
            catch (Exception exception)
            {
                _logger.WriteLog("Cannot Delete data \n-----------------\n" + exception);
                throw;
            }
        }

        public IEnumerable<Project> GetAllProject()
        {
            try
            {
                return _projectRepository.GetAllItem();
            }
            catch (Exception exception)
            {
                _logger.WriteLog("Cannot GetAll data \n-----------------\n" + exception);
                throw;
            }
        }

        public IEnumerable<Project> GetAllPagedProject(int pageSize, int pageNumber)
        {
            try
            {
                return _projectRepository.GetPagedAllItem(pageSize, pageNumber);
            }
            catch (Exception exception)
            {
                _logger.WriteLog("Cannot GetAllPaged data \n-----------------\n" + exception);
                throw;
            }
        }

        public Project GetProject(Guid id)
        {
            try
            {
                return _projectRepository.GetItem(id);
            }
            catch (Exception exception)
            {
                _logger.WriteLog("Cannot Get data \n-----------------\n" + exception);
                throw;
            }
        }

        public int GetTotalProject()
        {
            try
            {
                return _projectRepository.GetTotalCount();
            }
            catch (Exception exception)
            {
                _logger.WriteLog("Cannot GetTotal data \n-----------------\n" + exception);
                throw;
            }
        }

        public IEnumerable<Project> GetAllPagedProjectWithSearch(int pageSize, int pageNumber, Project project, string sortOrder)
        {
            try
            {
                return _projectRepository.GetPagedAllItem(pageSize, pageNumber, project, sortOrder);
            }
            catch (Exception exception)
            {
                _logger.WriteLog("Cannot GetAllPaged data \n-----------------\n" + exception);
                throw;
            }
        }

        public int GetTotalProjectWithSearch(Project project, string sortOrder)
        {
            try
            {
                return _projectRepository.GetTotalCount(project, sortOrder);
            }
            catch (Exception exception)
            {
                _logger.WriteLog("Cannot GetTotal data WithSearch \n-----------------\n" + exception);
                throw;
            }
        }

        #endregion

    }
}