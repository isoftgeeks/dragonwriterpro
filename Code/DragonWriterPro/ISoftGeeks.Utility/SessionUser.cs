﻿using System;

namespace ISoftGeeks.Utility
{
    public class SessionUser
    {
        public Guid ID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public int UserType { get; set; }
    }
}
