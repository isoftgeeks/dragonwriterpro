﻿using System;
using System.Reflection;
using log4net;

namespace ISoftGeeks.Utility
{
    public class Logger
    {
        private static readonly ILog log =
           LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public void WriteLog(string message)
        {
            try
            {
                log.Fatal(message);
            }
            catch (Exception ex)
            {
                log.Fatal(ex.Message);
            }

        }
    }
}
