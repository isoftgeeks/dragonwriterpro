﻿using System;
using System.Collections.Generic;
using System.Net.Mail;

namespace ISoftGeeks.Utility
{
    public class EmailSettings
    {
        public string Host { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public int Port { get; set; }
        public bool UseSSL { get; set; }
    }

    public class NameWithEmail
    {
        public string Name { get; set; }
        public string Email { get; set; }
    }

    public enum EmailTypeOptions
    {
        HtmlMail,
        HtmlMailWithImage,
        HtmlMailWithFile,
        TextMail
    }

    public class EmailSender
    {

        private SmtpClient _client;
        private MailMessage _mail;

        public string Subject { get; set; }
        public string Body { get; set; }
        public NameWithEmail Sender { get; set; }
        public List<NameWithEmail> ReplyToList { get; set; }
        public List<NameWithEmail> Receivers { get; set; }
        public MailPriority Priority { get; set; }
        public EmailTypeOptions EmailType { get; set; }
        public string Host { get; set; }

        public EmailSender(EmailSettings emailSettings)
        {
            _client = new SmtpClient();
            _client.Credentials = new System.Net.NetworkCredential(emailSettings.Username, emailSettings.Password);
            _client.Port = emailSettings.Port;
            _client.Host = emailSettings.Host;
            _client.EnableSsl = emailSettings.UseSSL;
        }

        public void SendMail()
        {
            try
            {
                _mail = new MailMessage();
                if (Receivers != null)
                {
                    foreach (var receiver in Receivers)
                    {
                        _mail.To.Add(new MailAddress(receiver.Email, receiver.Name, System.Text.Encoding.UTF8));
                    }
                }
                if (ReplyToList != null)
                {
                    foreach (var replayTo in ReplyToList)
                    {
                        _mail.ReplyToList.Add(new MailAddress(replayTo.Email, replayTo.Name, System.Text.Encoding.UTF8));
                    }
                }

                _mail.From = new MailAddress(Sender.Email, Sender.Name, System.Text.Encoding.UTF8);
                _mail.Subject = Subject;
                _mail.SubjectEncoding = System.Text.Encoding.UTF8;
                _mail.Body = Body;
                _mail.BodyEncoding = System.Text.Encoding.UTF8;

                switch (EmailType)
                {
                    case EmailTypeOptions.HtmlMail:
                    case EmailTypeOptions.HtmlMailWithFile:
                    case EmailTypeOptions.HtmlMailWithImage:
                        _mail.IsBodyHtml = true;
                        break;
                    case EmailTypeOptions.TextMail:
                        _mail.IsBodyHtml = false;
                        break;
                }

                _mail.Priority = Priority;
                _client.Send(_mail);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}