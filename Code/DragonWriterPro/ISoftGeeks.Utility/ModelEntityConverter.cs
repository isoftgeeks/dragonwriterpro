﻿using System.Reflection;

namespace ISoftGeeks.Utility
{
    public static class ModelEntityConverter
    {
        public static TModel BindEntityToModel<TEntity, TModel>(TEntity entity, TModel model)
        {
            var entityType = entity.GetType();
            var modelType = model.GetType();

            var entityProperties = entityType.GetProperties(BindingFlags.Public | BindingFlags.Instance);
            var modelProperties = modelType.GetProperties(BindingFlags.Public | BindingFlags.Instance);

            foreach (var entityProperty in entityProperties)
            {
                foreach (var modelProperty in modelProperties)
                {
                    if (entityProperty.Name == modelProperty.Name)
                    {
                        modelProperty.SetValue(model, entityProperty.GetValue(entity));
                        break;
                    }
                }
            }

            return model;
        }

        public static TEntity BindModelToEntity<TEntity, TModel>(TEntity entity, TModel model)
        {
            var entityType = entity.GetType();
            var modelType = model.GetType();

            var entityProperties = entityType.GetProperties(BindingFlags.Public | BindingFlags.Instance);
            var modelProperties = modelType.GetProperties(BindingFlags.Public | BindingFlags.Instance);

            foreach (var entityProperty in entityProperties)
            {
                foreach (var modelProperty in modelProperties)
                {
                    if (entityProperty.Name == modelProperty.Name)
                    {
                        entityProperty.SetValue(entity, modelProperty.GetValue(model));
                        break;
                    }
                }
            }

            return entity;
        }

        public static string CodeCourtesy()
        {
            return "Jalal Uddin, CEO, Proggasoft.";
        }
    }
}