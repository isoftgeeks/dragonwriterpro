﻿namespace ISoftGeeks.Utility
{
    public class NameWithPhone
    {
        public string Name { get; set; }
        public string Phone { get; set; }
    }

    public class SmsSettings
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }

    public class SmsSender
    {
        public void SendSms()
        {
        }
    }

    
}
