﻿using System;
using System.Reflection;
using log4net;

[assembly: log4net.Config.XmlConfigurator(ConfigFile = "Log4Net.config", Watch = true)]

namespace Dragon.Web
{
    public class Logger
    {
        private static readonly ILog log =
            LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public void WriteLog(string message)
        {
            try
            {
                log.Fatal(message);
            }
            catch (Exception ex)
            {
                log.Fatal(ex.Message);
            }
            
        }
    }
}