﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DragonWriterPro.Web.Startup))]
namespace DragonWriterPro.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
