﻿using DragonMember.Enums;
using ISoftGeeks.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DragonWriterPro.Web.Codes
{
    public class AuthorizedMember:AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            bool isMember = false;

            if (!httpContext.Request.IsAuthenticated)
                isMember = false;

            var user = UserSession.CurrentUser;
            if (user!=null)
            {
                if (user.UserType == (int)UserType.Member)
                {
                    isMember = true;
                    return isMember;
                }
            }

            // marina not selected
            HttpContext.Current.Response.BufferOutput = true;
            HttpContext.Current.Response.Redirect("/Home/UserLogin", true);
            HttpContext.Current.Response.Close();
            return isMember;
        }
    }
}