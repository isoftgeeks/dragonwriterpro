using System;
using DragonWriterPro.Web.App_Start;
using System.Collections.Generic;
using DragonMember;
using ISoftGeeks.Encryption;
using ISoftGeeks.Utility;
using ISoftGeeks.Notification;

namespace DragonWriterPro.Web.Models
{
    public class UserModel
    {
        #region Proparties
        public Guid ID { get; set; }
        public string EmailAddress { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public string Salt { get; set; }
        public string ImagePath { get; set; }
        public string ThambnailPath { get; set; }
        #endregion

        #region Constructors
        private IDragonMemberService _dragonMemberService;
        private PasswordManager _passwordManager;
        public UserModel()
        {
            _dragonMemberService = NinjectWebCommon.GetConcreteInstance<IDragonMemberService>();
            _passwordManager = new PasswordManager();
        }

        #endregion

        public void AddUser()
        {
            User user = NinjectWebCommon.GetConcreteInstance<User>();
            ModelEntityConverter.BindModelToEntity(user, this);
            user.ID = Guid.NewGuid();

            if (Password == ConfirmPassword)
            {
                var passworddetails = _passwordManager.SetPassword(Password);
                user.Password = passworddetails["Password"];
                user.Salt = passworddetails["Salt"];
                _dragonMemberService.AddUser(user);
            }
            else
            {
                Notification.AddErrorNotification("PasswordMismatch", "Password and Confirm Password Did Not Matched");
            }
        }

        public void EditUser()
        {
            User user = NinjectWebCommon.GetConcreteInstance<User>();
            ModelEntityConverter.BindModelToEntity(user, this);
            if (user.ID != Guid.Empty)
                _dragonMemberService.EditUser(user);
        }

        public void DeleteUser(Guid id)
        {
            _dragonMemberService.DeleteUser(id);
        }

        public UserModel GetUser(Guid id)
        {
            try
            {
                var entity = _dragonMemberService.GetUser(id);
                ModelEntityConverter.BindEntityToModel(entity, this);
                Password = _passwordManager.GetPassword(entity.Password,entity.Salt);
                return this;
            }
            catch
            {
                return null;
            }
        }

        public UserModel GetUser(string email)
        {
            try
            {
                var entity = _dragonMemberService.GetUserByEmail(email);
                ModelEntityConverter.BindEntityToModel(entity, this);
                Password = _passwordManager.GetPassword(entity.Password, entity.Salt);
                return this;
            }
            catch
            {
                return null;
            }

        }

        public IEnumerable<UserModel> GetAllUser()
        {
            var entityList = _dragonMemberService.GetAllUser();
            var modelList = new List<UserModel>(); foreach (var entity in entityList)
            {
                ModelEntityConverter.BindEntityToModel(entity, this);
                modelList.Add(this);
            }
            return modelList;
        }

        public IEnumerable<UserModel> GetAllPagedUser(int pageSize, int pageNumber)
        {
            var entityList = _dragonMemberService.GetAllPagedUser(pageSize, pageNumber);
            var modelList = new List<UserModel>(); foreach (var entity in entityList)
            {
                ModelEntityConverter.BindEntityToModel(entity, this);
                modelList.Add(this);
            }
            return modelList;
        }

        public int GetTotalUser()
        {
            return _dragonMemberService.GetTotalUser();
        }

        public IEnumerable<UserModel> GetAllPagedUserWithSearch(int pageSize, int pageNumber, User user, string sortOrder)
        {
            var entityList = _dragonMemberService.GetAllPagedUserWithSearch(pageSize, pageNumber, user, sortOrder);
            var modelList = new List<UserModel>(); foreach (var entity in entityList)
            {
                ModelEntityConverter.BindEntityToModel(entity, this);
                modelList.Add(this);
            }
            return modelList;
        }

        public int GetTotalUserWithSearch(User user, string sortOrder)
        {
            return _dragonMemberService.GetTotalUserWithSearch(user, sortOrder);
        }

        public string ConfirmPassword { get; set; }
    }
}