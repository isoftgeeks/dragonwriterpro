﻿using DragonMember;
using DragonMember.Enums;
using DragonWriterPro.Web.App_Start;
using ISoftGeeks.Utility;
using ISoftGeeks.Encryption;
using System;

namespace DragonWriterPro.Web.Models
{
    public class LogInModel
    {
        public string Email { get; set; }

        public string Password { get; set; }

        public bool RememberMe { get; set; }

        #region Constructors
        private IDragonMemberService _dragonMember;
        private PasswordManager _passwordManager;
        public LogInModel()
        {

            _dragonMember = NinjectWebCommon.GetConcreteInstance<IDragonMemberService>();
            _passwordManager = new PasswordManager();
        }

        #endregion

        internal LoginStatus UserLogin()
        {
            var user = _dragonMember.GetUserByEmail(this.Email);
            try
            {
                if (this.Password == _passwordManager.GetPassword(user.Password,user.Salt))
                {
                    var sessionUser = new SessionUser()
                    {
                        //Phone = user.Phone,
                        Email = user.EmailAddress,
                        Name = user.Name,
                        ID = user.ID,
                        //UserType = user.UserType
                    };

                    UserSession.User = sessionUser;
                    UserSession.AddToCookie(sessionUser);
                    return LoginStatus.Successful;
                }
                else
                {
                    return LoginStatus.Failed;
                }
            }
            catch (Exception)
            {
                return LoginStatus.Failed;
            }
        }
    
    }
}