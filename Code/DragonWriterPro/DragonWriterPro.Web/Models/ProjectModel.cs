using System;
using DragonWriterPro.Web.App_Start;
using System.Collections.Generic;
using DragonWriter;
using ISoftGeeks.Utility;

namespace DragonWriterPro.Web.Models
{
    public class ProjectModel
    {
        #region Proparties
        public Guid ID { get; set; }
        public string WebPath { get; set; }
        public string DatabaseServer { get; set; }
        public string DBUserName { get; set; }
        public string Pass { get; set; }
        public Guid UserID { get; set; }
        DateTime CreatedOn { get; set; }
        public string DatabaseName { get; set; }
        #endregion

        #region Constructors
        private IDragonWriterService _dragonWriterService;
        public ProjectModel()
        {
            _dragonWriterService = NinjectWebCommon.GetConcreteInstance<IDragonWriterService>();
        }

        #endregion

        public void AddProject()
        {
            Project project = NinjectWebCommon.GetConcreteInstance<Project>();
            ModelEntityConverter.BindModelToEntity(project, this);
            project.ID = Guid.NewGuid();
            _dragonWriterService.AddProject(project);
        }

        public void EditProject()
        {
            Project project = NinjectWebCommon.GetConcreteInstance<Project>();
            ModelEntityConverter.BindModelToEntity(project, this);
            if (project.ID != Guid.Empty)
                _dragonWriterService.EditProject(project);
        }

        public void DeleteProject(Guid id)
        {
            _dragonWriterService.DeleteProject(id);
        }

        public ProjectModel GetProject(Guid id)
        {
            var entity = _dragonWriterService.GetProject(id);
            ModelEntityConverter.BindEntityToModel(entity, this);
            return this;
        }

        public IEnumerable<ProjectModel> GetAllProject()
        {
            var entityList = _dragonWriterService.GetAllProject();
            var modelList = new List<ProjectModel>(); foreach (var entity in entityList)
            {
                ModelEntityConverter.BindEntityToModel(entity, this);
                modelList.Add(this);
            }
            return modelList;
        }

        public IEnumerable<ProjectModel> GetAllPagedProject(int pageSize, int pageNumber)
        {
            var entityList = _dragonWriterService.GetAllPagedProject(pageSize, pageNumber);
            var modelList = new List<ProjectModel>(); foreach (var entity in entityList)
            {
                ModelEntityConverter.BindEntityToModel(entity, this);
                modelList.Add(this);
            }
            return modelList;
        }

        public int GetTotalProject()
        {
            return _dragonWriterService.GetTotalProject();
        }

        public IEnumerable<ProjectModel> GetAllPagedProjectWithSearch(int pageSize, int pageNumber, string sortOrder)
        {
            var project = NinjectWebCommon.GetConcreteInstance<Project>();
            ModelEntityConverter.BindModelToEntity(project, this);

            var entityList = _dragonWriterService.GetAllPagedProjectWithSearch(pageSize, pageNumber, project, sortOrder);
            var modelList = new List<ProjectModel>(); foreach (var entity in entityList)
            {
                ModelEntityConverter.BindEntityToModel(entity, this);
                modelList.Add(this);
            }
            return modelList;
        }

        public int GetTotalProjectWithSearch(ProjectModel model, string sortOrder)
        {
            var project = NinjectWebCommon.GetConcreteInstance<Project>();
            ModelEntityConverter.BindModelToEntity(project, model);

            return _dragonWriterService.GetTotalProjectWithSearch(project, sortOrder);
        }

    }
}