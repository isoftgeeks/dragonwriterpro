﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DragonWriterPro.Web.Models
{
    public class ForgotPasswordModel
    {
        public string Email { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
    }
}