using System;
 using DragonWriterPro.Web.App_Start;using System.Collections.Generic;
using DragonWriter;
using ISoftGeeks.Utility;

namespace DragonWriterPro.Web.Models
{
	public class ModuleModel
	{
		#region Proparties
		public Guid ID { get; set; }
		public string ModulePath { get; set; }
		public string Namespace { get; set; }
		public Guid ProjectID { get; set; }
		DateTime CreatedOn { get; set; }
		#endregion

		#region Constructors
		private IDragonWriterService _dragonWriterService;
		public ModuleModel()
		{
			_dragonWriterService = NinjectWebCommon.GetConcreteInstance<IDragonWriterService>();}

		#endregion

		public void AddModule()
		{
			Module module = NinjectWebCommon.GetConcreteInstance<Module>();
			ModelEntityConverter.BindModelToEntity(module, this);
			module.ID = Guid.NewGuid();
			_dragonWriterService.AddModule(module);
			}

		public void EditModule()
		{
			Module module = NinjectWebCommon.GetConcreteInstance<Module>();
			ModelEntityConverter.BindModelToEntity(module, this);
			if(module.ID != Guid.Empty)
			_dragonWriterService.EditModule(module);
			}

		public void DeleteModule(Guid id)
		{
			_dragonWriterService.DeleteModule(id);
			}

		public ModuleModel GetModule(Guid id)
		{
			var entity = _dragonWriterService.GetModule(id);
			ModelEntityConverter.BindEntityToModel(entity, this);
			return this;
			}

		public  IEnumerable<ModuleModel> GetAllModule()
		{
			var entityList = _dragonWriterService.GetAllModule();
			var modelList = new List<ModuleModel>();foreach (var entity in entityList)
			{
			ModelEntityConverter.BindEntityToModel(entity, this);
				modelList.Add(this);
				}
			return modelList;
			}

		public  IEnumerable<ModuleModel> GetAllPagedModule(int pageSize, int pageNumber)
		{
			var entityList = _dragonWriterService.GetAllPagedModule(pageSize, pageNumber);
			var modelList = new List<ModuleModel>();foreach (var entity in entityList)
			{
			ModelEntityConverter.BindEntityToModel(entity, this);
				modelList.Add(this);
				}
			return modelList;
			}

		public int GetTotalModule()
		{
			return _dragonWriterService.GetTotalModule();
			}

        public IEnumerable<ModuleModel> GetAllPagedModuleWithSearch(int pageSize, int pageNumber, Module module, string sortOrder)
		{
			var entityList = _dragonWriterService.GetAllPagedModuleWithSearch(pageSize, pageNumber,module, sortOrder );
			var modelList = new List<ModuleModel>();foreach (var entity in entityList)
			{
			ModelEntityConverter.BindEntityToModel(entity, this);
				modelList.Add(this);
				}
			return modelList;
			}

		public int GetTotalModuleWithSearch(Module module, string sortOrder)
		{
			return _dragonWriterService.GetTotalModuleWithSearch(module, sortOrder);
			}

		
	}
}