﻿using DragonWriterPro.Web.Codes;
using DragonWriterPro.Web.Models;
using ISoftGeeks.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DragonWriterPro.Web.Controllers
{
    [AuthorizedMember]
    public class MemberController : Controller
    {
        public ActionResult ProjectList(Guid userId)
        {
            ProjectModel model = new ProjectModel();
            model.UserID = userId;
            var data = model.GetAllPagedProjectWithSearch(10, 1, "DBUserName ASC");
            //model = new ProjectModel();
            //model.UserID = userId;
            ViewBag.PageCount = model.GetTotalProjectWithSearch(model,"DBUserName ASC");
            return View(data); 
        }

        [HttpPost]
        public ActionResult ProjectList(Guid userId, int? pageSize, int? pageNumber)
        {
            ProjectModel model = new ProjectModel();
            model.UserID = userId;
            var data = model.GetAllPagedProjectWithSearch(10, 1, "DBUserName ASC");
            ViewBag.PageCount = model.GetTotalProjectWithSearch(model,"DBUserName ASC");

            return View(data);
        }

        public ActionResult AddProject()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddProject(ProjectModel model)
        {
            model.UserID = UserSession.CurrentUser.ID;
            model.AddProject();
            return RedirectToAction("ProjectList", new { userId = UserSession.CurrentUser.ID });
        }
        // GET: Member
        public ActionResult Profile(Guid userId)
        {
            UserModel umodel = new UserModel();
            var getuser = umodel.GetUser(userId);
            return View(getuser);
        }
    }
}