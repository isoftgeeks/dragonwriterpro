﻿using DragonMember.Enums;
using DragonWriterPro.Web.Codes;
using DragonWriterPro.Web.Models;
using ISoftGeeks.Notification;
using ISoftGeeks.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

//using Logger = Dragon.Web.Logger;

namespace DragonWriterPro.Web.Controllers
{
    public class HomeController : Controller
    {
        private Logger _logger;

        public HomeController()
        {
            _logger = new Logger();
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult NewMember()
        {
            return View();
        }

        [HttpPost]
        public ActionResult NewMember(UserModel model)
        {
            try
            {
                UserModel user = model.GetUser(model.EmailAddress);
                if (user == null)
                {
                    if (ModelState.IsValid)
                    {
                        try
                        {
                            model.AddUser();
                            Notification.AddSuccessNotification("AddUserSuccess", "User Added Successfully. You Can Log In Now.");
                            return RedirectToAction("UserLogin", "Home");
                        }
                        catch (Exception exception)
                        {
                            _logger.WriteLog(
                            string.Format("Failed to create user with parameters: FullName={0}, Password={1}, Email={2} \n ----------------------- \n " + exception, model.Name, model.Password, model.EmailAddress));
                            return View();
                        }
                    }
                    _logger.WriteLog(
                        string.Format("Failed to create user with parameters: FullName={0}, Password={1}, Email={2}", model.Name, model.Password, model.EmailAddress));
                }
                else
                {
                    Notification.AddErrorNotification("EmailAddress", "Email Address Already exits");
                }
            }
            catch (Exception ex)
            {
                if (string.IsNullOrEmpty(model.EmailAddress) && string.IsNullOrWhiteSpace(model.EmailAddress))
                {
                    Notification.AddErrorNotification("EmailAddress", "Email Address missing");
                }
                if (string.IsNullOrEmpty(model.Name) && string.IsNullOrWhiteSpace(model.Name))
                {
                    Notification.AddErrorNotification("UserName", "Name can not be empty");
                }
                if (string.IsNullOrEmpty(model.Password) && string.IsNullOrWhiteSpace(model.Password))
                {
                    Notification.AddErrorNotification("Password", "Password can not be empty");
                }

                _logger.WriteLog("Cannot insert data \n-----------------\n" + ex);
                throw;
            }
            return View();

        }


        public ActionResult UserLogin()
        {
            if (UserSession.CurrentUser != null)
                return RedirectToAction("Profile", "Member", new { userId = UserSession.CurrentUser.ID });
            return View();
        }

        [HttpPost]
        public ActionResult UserLogin(LogInModel model, string returnUrl)
        {
            if (model.UserLogin() == LoginStatus.Successful)
            {
                FormsAuthentication.SetAuthCookie(model.Email, model.RememberMe);

                if (string.IsNullOrEmpty(returnUrl))
                {
                    return RedirectToAction("Profile", "Member", new { userId = UserSession.CurrentUser.ID });
                }
                else
                {
                    return Redirect(returnUrl);
                }
            }
            else
            {
                Notification.AddErrorNotification("LogInFailed", "Invalid Phone Number or Password");
            }
            return View();
        }


        public ActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ForgotPassword(ForgotPasswordModel model)
        {
            UserModel user = new UserModel();
            try
            {
                user = user.GetUser(model.Email);
                if (user != null)
                {
                    EmailManager forgotEmail = new EmailManager();

                    //model.Phone = model.Phone;
                    model.Name = user.Name;
                    model.Password = user.Password;
                    forgotEmail.ForgotPassword(model);
                    Notification.AddSuccessNotification("ForgotPassword", "Your Password e-Mailed Successfully");
                }
                else
                {
                    Notification.AddErrorNotification("ForgotPassword", "no record found");
                }
            }
            catch (Exception ex)
            {
                Notification.AddErrorNotification("ForgotPassword", "Email Field can not be empty");
            }
            return View();
        }


        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            UserSession.Clear();
            return RedirectToAction("UserLogin", "Annonymous");
        }
    }
}