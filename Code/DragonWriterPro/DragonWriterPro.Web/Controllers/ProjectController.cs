﻿using System.Web.Mvc;
using DragonWriterPro.Web.Codes;
using DragonWriterPro.Web.Models;

namespace DragonWriterPro.Web.Controllers
{
    [AuthorizedMember]
    public class ProjectController : Controller
    {
        // GET: Project
        public ActionResult Index()
        {
            ProjectModel model = new ProjectModel();
            var data = model.GetAllPagedProject(5, 1);
            return View(data);
        }

        public ActionResult Create()
        {
            var model = new ProjectModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(ProjectModel model)
        {
            model.AddProject();
            return View(model);
        }

    }
}