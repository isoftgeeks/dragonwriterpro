﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Linq;

namespace ISoftGeeks.Notification
{
    public static class Notification
    {
        private static List<NotificationModel> notificationSummary = new List<NotificationModel>();

        public static void AddErrorNotification(string key, string message)
        {
            NotificationModel model = new NotificationModel();
            model.NotificationID = new Guid();
            model.Key = key;
            model.Message = message;
            model.TypeID = (int)NotificationModel.NotificationType.Error;
            notificationSummary.Add(model);
        }

        public static void AddSuccessNotification(string key, string message)
        {
            NotificationModel model = new NotificationModel();
            model.NotificationID = new Guid();
            model.Key = key;
            model.Message = message;
            model.TypeID = (int)NotificationModel.NotificationType.Success;
            notificationSummary.Add(model);
        }

        public static void AddWorningNotification(string key, string message)
        {
            NotificationModel model = new NotificationModel();
            model.NotificationID = new Guid();
            model.Key = key;
            model.Message = message;
            model.TypeID = (int)NotificationModel.NotificationType.Worning;
            notificationSummary.Add(model);
        }

        public static void Clear()
        {
            notificationSummary = null;
            notificationSummary = new List<NotificationModel>();
        }

        public static HtmlString NotificationMessage(string key)
        {
            return new HtmlString("<span class='validation-single'>" + notificationSummary.Where(i => i.Key == key).FirstOrDefault().Message + "</span>");
        }

        public static string SuccessNotificationSummary()
        {
            string message = "";
            var successNotificationSummery = notificationSummary.Where(i => i.TypeID == (int)NotificationModel.NotificationType.Success).ToList();
            if (successNotificationSummery.Count > 0)
            {
                message = "<div><ul>";

                foreach (var notification in successNotificationSummery)
                {
                    message += "<li>" + notification.Message + "</li>";
                }
                message += "</ul></div>";
            }
            return message;
        }

        public static string ErrorNotificationSummary()
        {
            string message = "";
            var successNotificationSummery = notificationSummary.Where(i => i.TypeID == (int)NotificationModel.NotificationType.Error).ToList();
            if (successNotificationSummery.Count > 0)
            {
                message = "<div><ul>";

                foreach (var notification in successNotificationSummery)
                {
                    message += "<li>" + notification.Message + "</li>";
                }
                message += "</ul></div>";
            }
            return message;
        }

        public static int NumberOfSuccessNotification()
        {
            return notificationSummary.Where(i => i.TypeID == (int)NotificationModel.NotificationType.Success).ToList().Count;
        }

        public static int NumberOfErrorNotification()
        {
            return notificationSummary.Where(i => i.TypeID == (int)NotificationModel.NotificationType.Error).ToList().Count;
        }
    }

}