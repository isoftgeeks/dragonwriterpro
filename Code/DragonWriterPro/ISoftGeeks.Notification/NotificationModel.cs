﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISoftGeeks.Notification
{
    public class NotificationModel
    {
        public Guid NotificationID { get; set; }
        public string Key { get; set; }
        public string Message { get; set; }
        public int TypeID { get; set; }

        public enum NotificationType
        {
            Success = 1,
            Worning,
            Error
        }

    }
}
