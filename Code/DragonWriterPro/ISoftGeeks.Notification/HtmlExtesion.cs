﻿using ISoftGeeks.Notification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.Mvc.Html
{
    public static class HtmlExtesion
    {
        public static MvcHtmlString SuccessNotificationSummary(this HtmlHelper html)
        {
            string msg = Notification.SuccessNotificationSummary();
            Notification.Clear();
            return new MvcHtmlString(msg);
        }

        public static int SuccessNotificationCount(this HtmlHelper html)
        {
            return Notification.NumberOfSuccessNotification();
        }

        public static MvcHtmlString ErrorNotificationSummary(this HtmlHelper html)
        {
            string msg = Notification.ErrorNotificationSummary();
            Notification.Clear();
            return new MvcHtmlString(msg);
        }

        public static int ErrorNotificationCount(this HtmlHelper html)
        {
            return Notification.NumberOfErrorNotification();
        }
    }
}