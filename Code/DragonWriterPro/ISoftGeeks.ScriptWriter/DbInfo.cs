using System;
using System.Collections.Generic;

namespace ISoftGeeks.ScriptWriter
{
    public class DbInfo
    {
        private readonly Settings _settings;

        public DbInfo(Settings settings)
        {
            _settings = settings;
        }

        public List<Table> GetTables()
        {
            _settings.Connect();
            List<Table> tables = new List<Table>();
            try
            {
                foreach (Microsoft.SqlServer.Management.Smo.Table myTable in _settings.Database.Tables)
                {
                    Table table = new Table();
                    table.Name = myTable.Name;
                    tables.Add(table);
                }

            }
            catch (Exception exception)
            {
                throw new Exception("Connection Error", exception);
            }
            _settings.Disconnect();
            return tables;
        }

        public List<StoredProcedure> GetStoredProcedures()
        {
            _settings.Connect();
            List<StoredProcedure> storedProcedures = new List<StoredProcedure>();
            try
            {
                foreach (Microsoft.SqlServer.Management.Smo.StoredProcedure storedProcedure in _settings.Database.StoredProcedures)
                {
                    if (storedProcedure.Schema == "dbo")
                    {
                        StoredProcedure sp = new StoredProcedure();
                        sp.Name = storedProcedure.Name;
                        storedProcedures.Add(sp);
                    }
                }
            }
            catch (Exception exception)
            {
                throw new Exception("Connection Error", exception);
            }
            _settings.Disconnect();
            return storedProcedures;
        }
    }
}