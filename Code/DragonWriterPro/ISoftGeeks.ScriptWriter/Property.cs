﻿namespace ISoftGeeks.ScriptWriter
{
    public class Property
    {
        public string Name { get; set; }
        public string DataType { get; set; }
        public int? Length { get; set; }
        public bool AllowNull { get; set; }
    }
}
