﻿using System.Collections.Generic;
using Microsoft.SqlServer.Management.Smo;

namespace ISoftGeeks.ScriptWriter
{
    public class EntityGenerator
    {
        private ColumnCollection _tableProparties;
        private readonly List<string> _baseEntityPropartyList = new List<string>();
        private readonly Settings _settings;

        public EntityGenerator(Settings settings)
        {
            _settings = settings;
        }

        public void GenerateEntity(string tableName)
        {
            CreateFolder();
            _baseEntityPropartyList.Add("ID");
            _baseEntityPropartyList.Add("CreatedOn");
            GenerateEntityBaseInterface();
            GenerateEntityBaseClass();
            GenerateInterface(tableName);
            GenerateClass(tableName);
        }

        private void CreateFolder()
        {
            //Create a new subfolder under the current active folder 
            string newPath = System.IO.Path.Combine(_settings.CodeBasePath, "Entity");

            // Create the subfolder
            System.IO.Directory.CreateDirectory(newPath);
        }

        private void GenerateEntityBaseClass()
        {
            string data = "using System;\nnamespace " + _settings.Namespace +
                          "\n\t { public class Entity : IEntity\n{\n\t\tpublic Guid ID { get; set; }\n\t\t" +
                          "public DateTime CreatedOn { get{return DateTime.UtcNow;} set{}  }\n\t}\n}";

            var writer = new System.IO.StreamWriter(_settings.CodeBasePath + "\\Entity\\Entity.cs");
            writer.Write(data);
            writer.Close();
        }

        private void GenerateEntityBaseInterface()
        {
            string data = "using System;\nnamespace " + _settings.Namespace +
                          "\n{\n\t public interface IEntity\n\t{\n\t\tGuid ID { get; set; }\n\t\t"+
                          "DateTime CreatedOn { get; set; }\n\t}\n}";

            var writer = new System.IO.StreamWriter(_settings.CodeBasePath + "\\Entity\\IEntity.cs");
            writer.Write(data);
            writer.Close();
        }

        private void GenerateInterface(string tableName)
        {
            _tableProparties = _settings.Server.Databases[_settings.DatabaseName].Tables[tableName, "dbo"].Columns;
            string data = "using System;\nnamespace " + _settings.Namespace + "\n{\n\t";

            data += "public interface I" + tableName + " : IEntity\n\t{\n\t\t";
            foreach (Column tableProparty in _tableProparties)
            {
                if (!_baseEntityPropartyList.Contains(tableProparty.Name))
                {
                    if (tableProparty.DataType.ToString() == "nvarchar")
                        data += "string " + tableProparty.Name + " { get; set; }\n\t\t";
                    else if (tableProparty.DataType.ToString() == "int")
                        data += "int " + tableProparty.Name + " { get; set; }\n\t\t";
                    else if (tableProparty.DataType.ToString() == "uniqueidentifier")
                        data += "Guid " + tableProparty.Name + " { get; set; }\n\t\t";
                    else if (tableProparty.DataType.ToString() == "datetime")
                        data += "DateTime " + tableProparty.Name + " { get; set; }\n\t\t";
                    else if (tableProparty.DataType.ToString() == "decimal")
                        data += "decimal " + tableProparty.Name + " { get; set; }\n\t\t";
                    else if (tableProparty.DataType.ToString() == "bit")
                        data += "bool " + tableProparty.Name + " { get; set; }\n\t\t";
                }
            }
            data += "\n\t}\n";
            data += "}";

            var writer = new System.IO.StreamWriter(_settings.CodeBasePath + "\\Entity\\I" + tableName + ".cs");
            writer.Write(data);
            writer.Close();
        }

        private void GenerateClass(string tableName)
        {
            _tableProparties = _settings.Server.Databases[_settings.DatabaseName].Tables[tableName, "dbo"].Columns;
            string data = "using System;\nnamespace " + _settings.Namespace + "\n{\n\t";

            data += "public class " + tableName + " : Entity, I" + tableName + "\n\t{\n\t\t";
            foreach (Column tableProparty in _tableProparties)
            {
                if (!_baseEntityPropartyList.Contains(tableProparty.Name))
                {
                    if (tableProparty.DataType.ToString() == "nvarchar")
                        data += "public string " + tableProparty.Name + " { get; set; }\n\t\t";
                    else if (tableProparty.DataType.ToString() == "int")
                        data += "public int " + tableProparty.Name + " { get; set; }\n\t\t";
                    else if (tableProparty.DataType.ToString() == "uniqueidentifier")
                        data += "public Guid " + tableProparty.Name + " { get; set; }\n\t\t";
                    else if (tableProparty.DataType.ToString() == "datetime")
                        data += "public DateTime " + tableProparty.Name + " { get; set; }\n\t\t";
                    else if (tableProparty.DataType.ToString() == "decimal")
                        data += "public decimal " + tableProparty.Name + " { get; set; }\n\t\t";
                    else if (tableProparty.DataType.ToString() == "bit")
                        data += "public bool " + tableProparty.Name + " { get; set; }\n\t\t";
                }
            }
            data += "\n\t}\n";
            data += "}";

            var writer = new System.IO.StreamWriter(_settings.CodeBasePath + "\\Entity\\" + tableName + ".cs");
            writer.Write(data);
            writer.Close();
        }
    }
}
