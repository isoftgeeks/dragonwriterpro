﻿using System;
using System.Collections.Generic;

namespace ISoftGeeks.ScriptWriter
{
    public class ServiceGenerator
    {
         private readonly Settings _settings;

         public ServiceGenerator(Settings settings)
        {
            _settings = settings;
        }

        private void CreateFolder()
        {
            //Create a new subfolder under the current active folder 
            string newPath = System.IO.Path.Combine(_settings.CodeBasePath, "Service");

            // Create the subfolder
            System.IO.Directory.CreateDirectory(newPath);
        }

        public void GenerateService(List<Table> tables)
        {
            CreateFolder();

            string interfaceData = "using System;\n" +
                                   "using System.Collections.Generic;\n\n" +
                                   "namespace " + _settings.Namespace + "\n{\n\t" +
                                   "public interface I" + _settings.Namespace + "Service\n\t{\n\t\t";


            string classData = "using System;\n" +
                                   "using System.Collections.Generic;\n"+
                                   "using ISoftGeeks.Utility;\n"+
                                   "using Ninject;\n\n" +
                                   "namespace " + _settings.Namespace + "\n{\n\t" +
                                   "public class " + _settings.Namespace + "Service:I" + _settings.Namespace + "Service\n\t{\n\t\t private Logger _logger; \n\t\t";

            //Generating DI parameters
            foreach (var table in tables)
            {
                if (table.IsChecked || table.IsCreateService)
                {
                    classData += "I" + table.Name + "Repository<" + table.Name + "> _" + Char.ToLowerInvariant(table.Name[0]) + table.Name.Substring(1) + "Repository;\n\t\t";
                }
            }
            classData += "\n";
            classData += "[Inject]\n\t\t public " + _settings.Namespace + "Service(";
            var firstItem = true;
            foreach (var table in tables)
            {
                if (firstItem)
                {
                    if (table.IsChecked || table.IsCreateService)
                    {
                        classData += "I" + table.Name + "Repository<" + table.Name + "> " + Char.ToLowerInvariant(table.Name[0]) + table.Name.Substring(1) + "Repository";
                        firstItem = false;
                    }
                }
                else
                {
                    if (table.IsChecked || table.IsCreateService)
                    {
                        classData += ",I" + table.Name + "Repository<" + table.Name + "> " + Char.ToLowerInvariant(table.Name[0]) + table.Name.Substring(1) + "Repository";
                    }
                }

            }

            classData += ")\n\t\t\t{\n\t\t\t\t";
            foreach (var table in tables)
            {
                if (table.IsChecked || table.IsCreateService)
                {
                    classData += "_" + Char.ToLowerInvariant(table.Name[0]) + table.Name.Substring(1) + "Repository = " + Char.ToLowerInvariant(table.Name[0]) + table.Name.Substring(1) + "Repository;\n\t\t\t";
                }
            }
            classData += "_logger = new Logger();";
            classData += "\n\t\t\t}\n\n\t\t";

            foreach (var table in tables)
            {
                if (table.IsChecked || table.IsCreateService)
                {
                    interfaceData = GenerateInterfaceMethod(table.Name, interfaceData);
                    classData = GenerateClass(table.Name, classData);
                }
            }

            interfaceData += "}\n}";
            var writer = new System.IO.StreamWriter(_settings.CodeBasePath + "\\Service\\I" + _settings.Namespace + "Service.cs");
            writer.Write(interfaceData);
            writer.Close();

            classData += "}\n}";
            writer = new System.IO.StreamWriter(_settings.CodeBasePath + "\\Service\\" + _settings.Namespace + "Service.cs");
            writer.Write(classData);
            writer.Close();
        }

        private string GenerateInterfaceMethod(string tableName, string interfaceData)
        {
            interfaceData += "#region " + tableName + "\n\t\t";
            interfaceData += "void Add" + tableName + "(" + tableName + " " + Char.ToLowerInvariant(tableName[0]) + tableName.Substring(1) + ");\n\t\t";
            interfaceData += "void Edit" + tableName + "(" + tableName + " " + Char.ToLowerInvariant(tableName[0]) + tableName.Substring(1) + ");\n\t\t";
            interfaceData += "void Delete" + tableName + "(Guid id);\n\t\t";
            interfaceData += tableName + " Get" + tableName + "(Guid id);\n\t\t";
            interfaceData += "IEnumerable<" + tableName + "> GetAll" + tableName + "();\n\t\t";
            interfaceData += "IEnumerable<" + tableName + "> GetAllPaged" + tableName + "(int pageSize, int pageNumber);\n\t\t";
            interfaceData += "int GetTotal" + tableName + "();\n\t\t";
            interfaceData += "IEnumerable<" + tableName + "> GetAllPaged" + tableName + "WithSearch(int pageSize, int pageNumber, " + tableName + " " + Char.ToLowerInvariant(tableName[0]) + tableName.Substring(1) + ", string sortOrder);\n\t\t";
            interfaceData += "int GetTotal" + tableName + "WithSearch(" + tableName + " " + Char.ToLowerInvariant(tableName[0]) + tableName.Substring(1) + ", string sortOrder);\n\t\t";

            interfaceData += "#endregion\n\n\t\t";

            return interfaceData;
        }

        private string GenerateClass(string tableName, string classData)
        {
            classData += "#region " + tableName + "\n\t\t";
            classData = GenerateAddItemMethodDefinition(tableName, classData);
            classData = GenerateEditItemMethodDefinition(tableName, classData);
            classData = GenerateDeleteItemMethodDefinition(tableName, classData);
            classData = GenerateAllItemMethodDefinition(tableName, classData);
            classData = GenerateAllItemPagedMethodDefinition(tableName, classData);
            classData = GenerateItemMethodDefinition(tableName, classData);
            classData = GenerateItemCountMethodDefinition(tableName, classData);
            classData = GenerateAllItemPagedWithSearchMethodDefinition(tableName, classData);
            classData = GenerateItemCountWithSearchMethodDefinition(tableName, classData);
            classData += "#endregion\n\n\t\t";

            return classData;
        }

        private string GenerateAddItemMethodDefinition(string tableName, string classData)
        {
            classData += "public void Add" + tableName + "(" + tableName + " " + Char.ToLowerInvariant(tableName[0]) + tableName.Substring(1) + ")\n\t\t" +
                         "{\n\t\t\t" +
                         "try\n\t\t\t{\n\t\t\t\t" +
                         "_" + Char.ToLowerInvariant(tableName[0]) + tableName.Substring(1) + "Repository.AddItem(" + Char.ToLowerInvariant(tableName[0]) + tableName.Substring(1) + ");\n\t\t\t\t}\n\t\t\t" +
                         "catch (Exception exception)\n\t\t\t{\n\t\t\t\t" +
                        "_logger.WriteLog(\"Cannot insert data \\n-----------------\\n\"+ exception);\n\t\t\t\tthrow;\n\t\t\t}\n\t\t}\n\n";

            return classData;
        }

        private string GenerateEditItemMethodDefinition(string tableName, string classData)
        {
            classData += "public void Edit" + tableName + "(" + tableName + " " + Char.ToLowerInvariant(tableName[0]) + tableName.Substring(1) + ")\n\t\t" +
                         "{\n\t\t\t" +
                         "try\n\t\t\t{\n\t\t\t\t" +
                         "_" + Char.ToLowerInvariant(tableName[0]) + tableName.Substring(1) + "Repository.UpdateItem(" + Char.ToLowerInvariant(tableName[0]) + tableName.Substring(1) + ");\n\t\t\t\t}\n\t\t\t" +
                         "catch (Exception exception)\n\t\t\t{\n\t\t\t\t" +
                         "_logger.WriteLog(\"Cannot Edit data \\n-----------------\\n\"+ exception);\n\t\t\t\tthrow;\n\t\t\t}\n\t\t}\n\n";


            return classData;
        }

        private string GenerateDeleteItemMethodDefinition(string tableName, string classData)
        {
            classData += "public void Delete" + tableName + "(Guid id)\n\t\t" +
                         "{\n\t\t\t" +
                         "try\n\t\t\t{\n\t\t\t\t" +
                         "_" + Char.ToLowerInvariant(tableName[0]) + tableName.Substring(1) + "Repository.DeleteItem(id);\n\t\t\t\t}\n\t\t\t" +
                         "catch (Exception exception)\n\t\t\t{\n\t\t\t\t" +
                        "_logger.WriteLog(\"Cannot Delete data \\n-----------------\\n\"+ exception);\n\t\t\t\tthrow;\n\t\t\t}\n\t\t}\n\n";

            return classData;
        }

        private string GenerateAllItemMethodDefinition(string tableName, string classData)
        {
            classData += "public IEnumerable<" + tableName + "> GetAll" + tableName + "()\n\t\t" +
                         "{\n\t\t\t" +
                         "try\n\t\t\t{\n\t\t\t\t" +
                         "return _" + Char.ToLowerInvariant(tableName[0]) + tableName.Substring(1) + "Repository.GetAllItem();\n\t\t\t\t}\n\t\t\t" +
                         "catch (Exception exception)\n\t\t\t{\n\t\t\t\t" +
                        "_logger.WriteLog(\"Cannot GetAll data \\n-----------------\\n\"+ exception);\n\t\t\t\tthrow;\n\t\t\t}\n\t\t}\n\n";

            return classData;
        }

        private string GenerateAllItemPagedMethodDefinition(string tableName, string classData)
        {
            classData += "public IEnumerable<" + tableName + "> GetAllPaged" + tableName + "(int pageSize, int pageNumber)\n\t\t" +
                         "{\n\t\t\t" +
                         "try\n\t\t\t{\n\t\t\t\t" +
                         "return  _" + Char.ToLowerInvariant(tableName[0]) + tableName.Substring(1) + "Repository.GetPagedAllItem(pageSize,pageNumber);\n\t\t\t\t}\n\t\t\t" +
                         "catch (Exception exception)\n\t\t\t{\n\t\t\t\t" +
                        "_logger.WriteLog(\"Cannot GetAllPaged data \\n-----------------\\n\"+ exception);\n\t\t\t\tthrow;\n\t\t\t}\n\t\t}\n\n";

            return classData;
        }

        private string GenerateItemMethodDefinition(string tableName, string classData)
        {
            classData += "public " + tableName + " Get" + tableName + "(Guid id)\n\t\t" +
                         "{\n\t\t\t" +
                         "try\n\t\t\t{\n\t\t\t\t" +
                         "return  _" + Char.ToLowerInvariant(tableName[0]) + tableName.Substring(1) + "Repository.GetItem(id);\n\t\t\t\t}\n\t\t\t" +
                         "catch (Exception exception)\n\t\t\t{\n\t\t\t\t" +
                        "_logger.WriteLog(\"Cannot Get data \\n-----------------\\n\"+ exception);\n\t\t\t\tthrow;\n\t\t\t}\n\t\t}\n\n";

            return classData;
        }

        private string GenerateItemCountMethodDefinition(string tableName, string classData)
        {
            classData += "public int GetTotal" + tableName + "()\n\t\t" +
                         "{\n\t\t\t" +
                         "try\n\t\t\t{\n\t\t\t\t" +
                         "return _" + Char.ToLowerInvariant(tableName[0]) + tableName.Substring(1) + "Repository.GetTotalCount();\n\t\t\t\t}\n\t\t\t" +
                         "catch (Exception exception)\n\t\t\t{\n\t\t\t\t" +
                        "_logger.WriteLog(\"Cannot GetTotal data \\n-----------------\\n\"+ exception);\n\t\t\t\tthrow;\n\t\t\t}\n\t\t}\n\n";

            return classData;
        }

        private string GenerateAllItemPagedWithSearchMethodDefinition(string tableName, string classData)
        {
            classData += "public IEnumerable<" + tableName + "> GetAllPaged" + tableName + "WithSearch(int pageSize, int pageNumber, " + tableName + " " + Char.ToLowerInvariant(tableName[0]) + tableName.Substring(1) + ", string sortOrder)\n\t\t" +
                         "{\n\t\t\t" +
                         "try\n\t\t\t{\n\t\t\t\t" +
                         "return _" + Char.ToLowerInvariant(tableName[0]) + tableName.Substring(1) + "Repository.GetPagedAllItem(pageSize, pageNumber," + Char.ToLowerInvariant(tableName[0]) + tableName.Substring(1) + ", sortOrder );\n\t\t\t\t}\n\t\t\t" +
                         "catch (Exception exception)\n\t\t\t{\n\t\t\t\t" +
                        "_logger.WriteLog(\"Cannot GetAllPaged data \\n-----------------\\n\"+ exception);\n\t\t\t\tthrow;\n\t\t\t}\n\t\t}\n\n";

            return classData;
        }

        private string GenerateItemCountWithSearchMethodDefinition(string tableName, string classData)
        {
            classData += "public int GetTotal" + tableName + "WithSearch(" + tableName + " " + Char.ToLowerInvariant(tableName[0]) + tableName.Substring(1) + ", string sortOrder)\n\t\t" +
                         "{\n\t\t\t" +
                         "try\n\t\t\t{\n\t\t\t\t" +
                         "return _" + Char.ToLowerInvariant(tableName[0]) + tableName.Substring(1) + "Repository.GetTotalCount(" + Char.ToLowerInvariant(tableName[0]) + tableName.Substring(1) + ", sortOrder);\n\t\t\t\t}\n\t\t\t" +
                         "catch (Exception exception)\n\t\t\t{\n\t\t\t\t" +
                        "_logger.WriteLog(\"Cannot GetTotal data WithSearch \\n-----------------\\n\"+ exception);\n\t\t\t\tthrow;\n\t\t\t}\n\t\t}\n\n";

            return classData;
        }

    }
}
