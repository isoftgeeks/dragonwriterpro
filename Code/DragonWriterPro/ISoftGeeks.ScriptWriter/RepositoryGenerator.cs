﻿namespace ISoftGeeks.ScriptWriter
{
    public class RepositoryGenerator
    {
        private readonly Settings _settings;

        public RepositoryGenerator(Settings settings)
        {
            _settings = settings;
        }

        private void CreateFolder()
        {
            //Create a new subfolder under the current active folder 
            string newPath = System.IO.Path.Combine(_settings.CodeBasePath, "Repository");

            // Create the subfolder
            System.IO.Directory.CreateDirectory(newPath);
        }

        private void GenerateInterface(string tableName)
        {
            string data = "using ISoftGeeks.Data;\nnamespace " + _settings.Namespace +
                          "\n {\n\t public interface I" + tableName +
                          "Repository<TEntity> : IRepository<TEntity>\n\t{\n\t}\n}";
            var writer = new System.IO.StreamWriter(_settings.CodeBasePath + "\\Repository\\I" + tableName + "Repository.cs");
            writer.Write(data);
            writer.Close();
        }

        private void GenerateClass(string tableName)
        {
            string data = "using ISoftGeeks.Data;\nnamespace " + _settings.Namespace +
                          "\n {\n\t public class " + tableName +
                          "Repository<TEntity> : Repository<TEntity>,I" + tableName + "Repository<TEntity>\n\t{\n\t"+
                          "public " + tableName + "Repository(string dbConnection): base(dbConnection){ }"
                          +"}\n}";

            var writer = new System.IO.StreamWriter(_settings.CodeBasePath + "\\Repository\\" + tableName + "Repository.cs");
            writer.Write(data);
            writer.Close();
        }

        public void GenerateRepository(string tableName)
        {
            CreateFolder();
            GenerateInterface(tableName);
            GenerateClass(tableName);
        }
    }
}
