﻿namespace ISoftGeeks.ScriptWriter
{
    public class ControllerGenerator
    {
        private readonly Settings _settings;

        public ControllerGenerator(Settings settings)
        {
            _settings = settings;
        }

        public  void GenerateModel(string tableName)
        {
            string data = "using System.Collections.Generic;\nusing " + _settings.WebNamespace + ".Models;\nusing Ninject;\n\nnamespace " + _settings.WebNamespace + ".Controllers\n{\n\t";
            data += "public class " + _settings.Namespace + "Controller:Controller\n\t{\n\t\t";

            data += "\n\t}\n";
            data += "}";

            var writer = new System.IO.StreamWriter(_settings.WebSolutionPath + "\\Controllers\\" + _settings.WebNamespace + "Controller.cs");
            writer.Write(data);
            writer.Close();
        }
    }
}
