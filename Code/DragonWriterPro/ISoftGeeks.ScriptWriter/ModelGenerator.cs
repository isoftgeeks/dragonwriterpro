﻿using System;
using Microsoft.SqlServer.Management.Smo;

namespace ISoftGeeks.ScriptWriter
{
    public class ModelGenerator
    {
        private ColumnCollection _tableProparties;
        private readonly Settings _settings;

        public ModelGenerator(Settings settings)
        {
            _settings = settings;
        }

        public void GenerateModel(string tableName)
        {
            string data = "using System;\n using " + _settings.WebNamespace + ".App_Start;using System.Collections.Generic;\nusing " + _settings.Namespace + ";\nusing ISoftGeeks.Utility;\n\nnamespace " + _settings.WebNamespace + ".Models\n{\n\t";
            data += "public class " + tableName + "Model\n\t{\n\t\t";
            data = GenerateProparties(tableName, data);
            data = GenerateConstractor(tableName, data);
            data = GenerateAddMetheod(tableName, data);
            data = GenerateEditMetheod(tableName, data);
            data = GenerateDeleteMetheod(tableName, data);
            data = GenerateGetMetheod(tableName, data);
            data = GenerateGetAllMetheod(tableName, data);
            data = GenerateGetAllPagedMetheod(tableName, data);
            data = GenerateGetTotalMetheod(tableName, data);
            data = GenerateGetAllPagedMetheodWithSearch(tableName, data);
            data = GenerateGetTotalMetheodWithSearch(tableName, data);
            data += "\n\t}\n";
            data += "}";

            var writer = new System.IO.StreamWriter(_settings.WebSolutionPath + "\\Models\\" + tableName + "Model.cs");
            writer.Write(data);
            writer.Close();
        }

        private string GenerateProparties(string tableName, string data)
        {
            data += "#region Proparties\n\t\t";
            _tableProparties = _settings.Server.Databases[_settings.DatabaseName].Tables[tableName, "dbo"].Columns;
            foreach (Column tableProparty in _tableProparties)
            {
                if (tableProparty.DataType.ToString() == "nvarchar")
                    data += "public string " + tableProparty.Name + " { get; set; }\n\t\t";
                else if (tableProparty.DataType.ToString() == "int")
                    data += "public int " + tableProparty.Name + " { get; set; }\n\t\t";
                else if (tableProparty.DataType.ToString() == "uniqueidentifier")
                    data += "public Guid " + tableProparty.Name + " { get; set; }\n\t\t";
                else if (tableProparty.DataType.ToString() == "datetime")
                    data += "DateTime " + tableProparty.Name + " { get; set; }\n\t\t";
                else if (tableProparty.DataType.ToString() == "decimal")
                    data += "public decimal " + tableProparty.Name + " { get; set; }\n\t\t";
            }
            data += "#endregion\n\n\t\t";
            return data;
        }

        private string GenerateConstractor(string tableName, string data)
        {
            data += "#region Constructors\n\t\t";
            data += "private I" + _settings.Namespace + "Service _" + Char.ToLowerInvariant(_settings.Namespace[0]) + _settings.Namespace.Substring(1) + "Service;\n\t\t";

            data += "public " + tableName + "Model()\n\t\t{\n\t\t\t";
            data += "_" + Char.ToLowerInvariant(_settings.Namespace[0]) + _settings.Namespace.Substring(1) + "Service = NinjectWebCommon.GetConcreteInstance<I" + _settings.Namespace + "Service>();";
            data += "}\n\n\t\t";
            data += "#endregion\n\n\t\t";
            return data;
        }

        private string GenerateAddMetheod(string tableName, string data)
        {
            data += "public void Add" + tableName + "()\n\t\t{\n\t\t\t" +
                    tableName + " " + Char.ToLowerInvariant(tableName[0]) + tableName.Substring(1) + " = NinjectWebCommon.GetConcreteInstance<" + tableName + ">();\n\t\t\t" +
                    "ModelEntityConverter.BindModelToEntity(" + Char.ToLowerInvariant(tableName[0]) + tableName.Substring(1) + ", this);\n\t\t\t" +
                    Char.ToLowerInvariant(tableName[0]) + tableName.Substring(1) + ".ID = Guid.NewGuid();\n\t\t\t" +
                    "_" + Char.ToLowerInvariant(_settings.Namespace[0]) + _settings.Namespace.Substring(1) + "Service.Add" + tableName + "(" + Char.ToLowerInvariant(tableName[0]) + tableName.Substring(1) + ");\n\t\t\t" +
                    "}\n\n\t\t";

            return data;
        }

        private string GenerateEditMetheod(string tableName, string data)
        {
            data += "public void Edit" + tableName + "()\n\t\t{\n\t\t\t" +
                    tableName + " " + Char.ToLowerInvariant(tableName[0]) + tableName.Substring(1) + " = NinjectWebCommon.GetConcreteInstance<" + tableName + ">();\n\t\t\t" +
                    "ModelEntityConverter.BindModelToEntity(" + Char.ToLowerInvariant(tableName[0]) + tableName.Substring(1) + ", this);\n\t\t\t" +
                    "if(" + Char.ToLowerInvariant(tableName[0]) + tableName.Substring(1) + ".ID != Guid.Empty)\n\t\t\t" +
                    "_" + Char.ToLowerInvariant(_settings.Namespace[0]) + _settings.Namespace.Substring(1) + "Service.Edit" + tableName + "(" + Char.ToLowerInvariant(tableName[0]) + tableName.Substring(1) + ");\n\t\t\t" +
                    "}\n\n\t\t";
            return data;
        }

        private string GenerateDeleteMetheod(string tableName, string data)
        {
            data += "public void Delete" + tableName + "(Guid id)\n\t\t{\n\t\t\t" +
                    "_" + Char.ToLowerInvariant(_settings.Namespace[0]) + _settings.Namespace.Substring(1) + "Service.Delete" + tableName + "(id);\n\t\t\t" +
                    "}\n\n\t\t";
            return data;
        }

        private string GenerateGetMetheod(string tableName, string data)
        {
            data += "public " + tableName + "Model Get" + tableName + "(Guid id)\n\t\t{\n\t\t\t" +
                    "var entity = _" + Char.ToLowerInvariant(_settings.Namespace[0]) + _settings.Namespace.Substring(1) + "Service.Get" + tableName + "(id);\n\t\t\t" +
                    "ModelEntityConverter.BindEntityToModel(entity, this);\n\t\t\t" +
                    "return this;\n\t\t\t" +
                    "}\n\n\t\t";
            return data;
        }

        private string GenerateGetAllMetheod(string tableName, string data)
        {
            data += "public  IEnumerable<" + tableName + "Model> GetAll" + tableName + "()\n\t\t{\n\t\t\t" +
                     "var entityList = _" + Char.ToLowerInvariant(_settings.Namespace[0]) + _settings.Namespace.Substring(1) + "Service.GetAll" + tableName + "();\n\t\t\t" +
                     "var modelList = new List<" + tableName + "Model>();" +
                     "foreach (var entity in entityList)\n\t\t\t" +
                    "{\n\t\t\t" +
                    "ModelEntityConverter.BindEntityToModel(entity, this);\n\t\t\t\t" +
                    "modelList.Add(this);\n\t\t\t\t" +
                    "}\n\t\t\t" +
                     "return modelList;\n\t\t\t" +
                     "}\n\n\t\t";
            return data;
        }

        private string GenerateGetAllPagedMetheod(string tableName, string data)
        {
            data += "public  IEnumerable<" + tableName + "Model> GetAllPaged" + tableName + "(int pageSize, int pageNumber)\n\t\t{\n\t\t\t" +
                     "var entityList = _" + Char.ToLowerInvariant(_settings.Namespace[0]) + _settings.Namespace.Substring(1) + "Service.GetAllPaged" + tableName + "(pageSize, pageNumber);\n\t\t\t" +
                     "var modelList = new List<" + tableName + "Model>();" +
                     "foreach (var entity in entityList)\n\t\t\t" +
                    "{\n\t\t\t" +
                    "ModelEntityConverter.BindEntityToModel(entity, this);\n\t\t\t\t" +
                    "modelList.Add(this);\n\t\t\t\t" +
                    "}\n\t\t\t" +
                     "return modelList;\n\t\t\t" +
                     "}\n\n\t\t";
            return data;
        }

        private string GenerateGetTotalMetheod(string tableName, string data)
        {
            data += "public int GetTotal" + tableName + "()\n\t\t{\n\t\t\t" +
                    "return _" + Char.ToLowerInvariant(_settings.Namespace[0]) + _settings.Namespace.Substring(1) + "Service.GetTotal" + tableName + "();\n\t\t\t" +
                    "}\n\n\t\t";
            return data;
        }

        //Change 

        private string GenerateGetAllPagedMetheodWithSearch(string tableName, string data)
        {
            data += "public  IEnumerable<" + tableName + "Model> GetAllPaged" + tableName + "WithSearch(int pageSize, int pageNumber, Dictionary<string, string> sortOrder)\n\t\t{\n\t\t\t" +
                     "var entityList = _" + Char.ToLowerInvariant(_settings.Namespace[0]) + _settings.Namespace.Substring(1) + "Service.GetAllPaged" + tableName + "WithSearch(pageSize, pageNumber," + Char.ToLowerInvariant(tableName[0]) + tableName.Substring(1) + ", sortOrder );\n\t\t\t" +
                     "var modelList = new List<" + tableName + "Model>();" +
                     "foreach (var entity in entityList)\n\t\t\t" +
                    "{\n\t\t\t" +
                    "ModelEntityConverter.BindEntityToModel(entity, this);\n\t\t\t\t" +
                    "modelList.Add(this);\n\t\t\t\t" +
                    "}\n\t\t\t" +
                     "return modelList;\n\t\t\t" +
                     "}\n\n\t\t";
            return data;
        }

        private string GenerateGetTotalMetheodWithSearch(string tableName, string data)
        {
            data += "public int GetTotal" + tableName + "WithSearch(" + tableName + "Dictionary<string, string> sortOrder)\n\t\t{\n\t\t\t" +
                "var " + Char.ToLowerInvariant(tableName[0]) + tableName.Substring(1) + " = NinjectWebCommon.GetConcreteInstance<" + tableName + ">();\n\t\t\t" +
                "ModelEntityConverter.BindModelToEntity(" + Char.ToLowerInvariant(tableName[0]) + tableName.Substring(1) + ", this);" +
                    "return _" + Char.ToLowerInvariant(_settings.Namespace[0]) + _settings.Namespace.Substring(1) + "Service.GetTotal" + tableName + "WithSearch(" + Char.ToLowerInvariant(tableName[0]) + tableName.Substring(1) + ", sortOrder);\n\t\t\t" +
                    "}\n\n\t\t";
            return data;
        }
    }
}
