﻿using System;
namespace ISoftGeeks.ScriptWriter
{
    public class Table
    {
        public Guid ID { get; set; }
        public string Name { get; set; }
        public bool IsChecked { get; set; }
        public bool IsCreateStoredProcedure { get; set; }
        public bool IsCreateRepository { get; set; }
        public bool IsCreateService { get; set; }
        public bool IsCreateController { get; set; }
        public bool IsCreateView { get; set; }
    }
}
