﻿using System;
using Microsoft.SqlServer.Management.Smo;

namespace ISoftGeeks.ScriptWriter
{
    public class StoredProcedureGenerator
    {
        private ColumnCollection _tableProparties;
        private readonly Settings _settings;

        public StoredProcedureGenerator(Settings settings)
        {
            _settings = settings;
        }

        private void SetParameters(ColumnCollection tableProparties, Microsoft.SqlServer.Management.Smo.StoredProcedure storedProcedure)
        {
            StoredProcedureParameter param;
            foreach (Column tableProparty in tableProparties)
            {
                param = new StoredProcedureParameter(storedProcedure, "@" + tableProparty.Name, tableProparty.DataType);
                storedProcedure.Parameters.Add(param);
            }
        }
        private void SetParametersForSearch(ColumnCollection tableProparties, Microsoft.SqlServer.Management.Smo.StoredProcedure storedProcedure)
        {
            StoredProcedureParameter param;
            foreach (Column tableProparty in tableProparties)
            {
                param = new StoredProcedureParameter(storedProcedure, "@" + tableProparty.Name, tableProparty.DataType);
                param.DefaultValue = "Null";
                storedProcedure.Parameters.Add(param);

            }
        }

        private void GenerateAddItemScript(string tableName)
        {
            _tableProparties = _settings.Server.Databases[_settings.DatabaseName].Tables[tableName, "dbo"].Columns;

            var storedProcedure =
                new Microsoft.SqlServer.Management.Smo.StoredProcedure(_settings.Database, tableName + "_Add" + tableName, "dbo");

            if (_settings.Database.StoredProcedures.Contains(storedProcedure.Name))
            {
                storedProcedure.Refresh();
                storedProcedure.Drop();
            }

            storedProcedure =
                new Microsoft.SqlServer.Management.Smo.StoredProcedure(_settings.Database, tableName + "_Add" + tableName, "dbo");
            storedProcedure.TextMode = false;
            storedProcedure.AnsiNullsStatus = false;
            storedProcedure.QuotedIdentifierStatus = false;

            SetParameters(_tableProparties, storedProcedure);

            var script = "BEGIN\nDeclare @IDCount [int];\n\nSELECT @IDCount = COUNT([ID]) FROM [dbo].[" + tableName +
                      "]\nWHERE [ID] = @ID\n\n";

            script += "IF @IDCount = 0\n\tBEGIN\n\t\tINSERT INTO [dbo].[" + tableName + "]\n\t\t\t(\n\t\t\t\t";

            bool firstItem = true;
            foreach (Column tableProparty in _tableProparties)
            {
                if (firstItem)
                {
                    script += "[" + tableProparty.Name + "]\n\t\t\t\t";
                    firstItem = false;
                }
                else
                {
                    script += ",[" + tableProparty.Name + "]\n\t\t\t\t";
                }
            }
            script += ")\n\t\t\tVALUES(";


            firstItem = true;
            foreach (Column tableProparty in _tableProparties)
            {
                if (firstItem)
                {
                    script += "@" + tableProparty.Name + "\n";
                    firstItem = false;
                }
                else
                {
                    script += "\t\t\t\t,@" + tableProparty.Name + "\n";
                }
            }

            script += "\t\t\t)\n\tEND\nEND";
            storedProcedure.TextBody = script;
            try
            {
                storedProcedure.Create();
            }
            catch (Exception e)
            {
                throw new Exception("StoredProcedure Creation Error", e);
            }
        }
        private void GenerateUpdateItemScript(string tableName)
        {
            _tableProparties = _settings.Server.Databases[_settings.DatabaseName].Tables[tableName, "dbo"].Columns;
            var storedProcedure =
                new Microsoft.SqlServer.Management.Smo.StoredProcedure(_settings.Database, tableName + "_Edit" + tableName, "dbo");

            if (_settings.Database.StoredProcedures.Contains(storedProcedure.Name))
            {
                storedProcedure.Refresh();
                storedProcedure.Drop();
            }

            storedProcedure =
                new Microsoft.SqlServer.Management.Smo.StoredProcedure(_settings.Database, tableName + "_Edit" + tableName, "dbo");
            storedProcedure.TextMode = false;
            storedProcedure.AnsiNullsStatus = false;
            storedProcedure.QuotedIdentifierStatus = false;

            SetParameters(_tableProparties, storedProcedure);

            var script = "BEGIN\nDeclare @IDCount [int];\n\nSELECT @IDCount = COUNT([ID]) FROM [dbo].[" + tableName +
                      "]\nWHERE [ID] = @ID\n\n";

            script += "IF @IDCount > 0\n\tBEGIN\n\t\tUPDATE [dbo].[" + tableName + "]\n\t\t\tSET\n\t\t\t\t";

            var firstItem = true;
            int rowNumber = 0;
            foreach (Column tableProparty in _tableProparties)
            {
                if (firstItem && rowNumber == 1)
                {
                    script += "[" + tableProparty.Name + "]=@" + tableProparty.Name + "\n";
                    firstItem = false;
                }
                else if (!firstItem)
                {
                    script += "\t\t\t\t,[" + tableProparty.Name + "]=@" + tableProparty.Name + "\n";

                }
                rowNumber++;
            }

            script += "\t\t\tWHERE [ID]=@ID\t\t\t\n\tEND\nEND";

            storedProcedure.TextBody = script;
            try
            {
                storedProcedure.Create();
            }
            catch (Exception e)
            {
                throw new Exception("StoredProcedure Creation Error", e);
            }
        }
        private void GenerateDeleteItemScript(string tableName)
        {
            var storedProcedure =
                new Microsoft.SqlServer.Management.Smo.StoredProcedure(_settings.Database, tableName + "_Delete" + tableName, "dbo");

            if (_settings.Database.StoredProcedures.Contains(storedProcedure.Name))
            {
                storedProcedure.Refresh();
                storedProcedure.Drop();
            }
            StoredProcedureParameter param;

            storedProcedure =
                new Microsoft.SqlServer.Management.Smo.StoredProcedure(_settings.Database, tableName + "_Delete" + tableName, "dbo");

            storedProcedure.TextMode = false;
            storedProcedure.AnsiNullsStatus = false;
            storedProcedure.QuotedIdentifierStatus = false;

            param = new StoredProcedureParameter(storedProcedure, "@ID", DataType.UniqueIdentifier);
            storedProcedure.Parameters.Add(param);
            var script = "BEGIN\nSET NOCOUNT ON\n\tDELETE\n\t\tFROM [dbo].[" + tableName + "] \n\tWHERE ID = @ID\nEND";
            storedProcedure.TextBody = script;
            try
            {
                storedProcedure.Create();
            }
            catch (Exception e)
            {
                throw new Exception("StoredProcedure Creation Error", e);
            }
        }
        private void GenerateGetItemScript(string tableName)
        {
            var storedProcedure =
                new Microsoft.SqlServer.Management.Smo.StoredProcedure(_settings.Database, tableName + "_Get" + tableName + "ByID", "dbo");

            if (_settings.Database.StoredProcedures.Contains(storedProcedure.Name))
            {
                storedProcedure.Refresh();
                storedProcedure.Drop();
            }
            StoredProcedureParameter param;

            storedProcedure =
                new Microsoft.SqlServer.Management.Smo.StoredProcedure(_settings.Database, tableName + "_Get" + tableName + "ByID", "dbo");

            storedProcedure.TextMode = false;
            storedProcedure.AnsiNullsStatus = false;
            storedProcedure.QuotedIdentifierStatus = false;

            param = new StoredProcedureParameter(storedProcedure, "@ID", DataType.UniqueIdentifier);
            storedProcedure.Parameters.Add(param);

            var script = "BEGIN\nSET NOCOUNT ON\n\tSELECT *\n\tFROM [dbo].[" + tableName + "]\n\tWHERE ID = @ID\nEND";
            storedProcedure.TextBody = script;
            try
            {
                storedProcedure.Create();
            }
            catch (Exception e)
            {
                throw new Exception("StoredProcedure Creation Error", e);
            }
        }
        private void GenerateGetAllItemScript(string tableName)
        {
            var storedProcedure =
                new Microsoft.SqlServer.Management.Smo.StoredProcedure(_settings.Database, tableName + "_GetAll" + tableName, "dbo");

            if (_settings.Database.StoredProcedures.Contains(storedProcedure.Name))
            {
                storedProcedure.Refresh();
                storedProcedure.Drop();
            }

            storedProcedure =
                new Microsoft.SqlServer.Management.Smo.StoredProcedure(_settings.Database, tableName + "_GetAll" + tableName, "dbo");

            storedProcedure.TextMode = false;
            storedProcedure.AnsiNullsStatus = false;
            storedProcedure.QuotedIdentifierStatus = false;

            var script = "BEGIN\nSET NOCOUNT ON\n\tSELECT *\n\tFROM [" + tableName + "]\nEND";
            storedProcedure.TextBody = script;
            try
            {
                storedProcedure.Create();
            }
            catch (Exception e)
            {
                throw new Exception("StoredProcedure Creation Error", e);
            }
        }
        private void GenerateGetAllPagedItemScript(string tableName)
        {
            ColumnCollection tableProparties = _settings.Server.Databases[_settings.DatabaseName].Tables[tableName, "dbo"].Columns;

            var storedProcedure =
                new Microsoft.SqlServer.Management.Smo.StoredProcedure(_settings.Database, tableName + "_GetAllPaged" + tableName, "dbo");

            if (_settings.Database.StoredProcedures.Contains(storedProcedure.Name))
            {
                storedProcedure.Refresh();
                storedProcedure.Drop();
            }
            StoredProcedureParameter param;

            storedProcedure =
                new Microsoft.SqlServer.Management.Smo.StoredProcedure(_settings.Database, tableName + "_GetAllPaged" + tableName, "dbo");

            storedProcedure.TextMode = false;
            storedProcedure.AnsiNullsStatus = true;
            storedProcedure.QuotedIdentifierStatus = false;

            param = new StoredProcedureParameter(storedProcedure, "@PageNumber", DataType.Int);
            storedProcedure.Parameters.Add(param);
            param = new StoredProcedureParameter(storedProcedure, "@PageSize", DataType.Int);
            storedProcedure.Parameters.Add(param);
            param = new StoredProcedureParameter(storedProcedure, "@SortOrder", DataType.NVarChar(500));
            storedProcedure.Parameters.Add(param);

            SetParametersForSearch(_tableProparties, storedProcedure);

            var script = "BEGIN\n\tSET NOCOUNT ON\n\n\tDECLARE @sql nvarchar(max),\n\t" +
                "@paramlist  nvarchar(4000),\n\t@CurrentRowIndex int;\n\n\t";

            script += "SET @CurrentRowIndex = ((@PageNumber - 1) * @PageSize) + 1;\n\n\t";

            script +=
                "SET @sql='\n\t\tWITH Custom_PageSegment AS (\n\t\t\tSELECT *," +
                "\n\t\t\tROW_NUMBER() OVER ( ORDER BY ' + @SortOrder + ' ) as RowIndex\n\t\t\t" +
                "FROM [dbo].[" + tableName + "] sm\n\t\t\tWHERE 1=1'\n\n\t\t";

            //TO DO:LOOP parameters
            foreach (Column tableProparty in tableProparties)
            {
                if (tableProparty.DataType.ToString() == "nvarchar")
                {
                    script += "IF @" + tableProparty.Name +
                        " IS NOT NULL\n\t\tSET @sql = @sql + ' AND sm." + tableProparty.Name +
                        "LIKE ''%'' + @x" + tableProparty.Name + "+ ''%'''\n\t\t";
                }
                else
                {
                    script += "IF @" + tableProparty.Name +
                        " IS NOT NULL\n\t\tSET @sql = @sql + ' AND sm." + tableProparty.Name +
                        "= @x" + tableProparty.Name + "'\n\t\t";
                }

            }
            //---------------------

            script += "\nSET @sql = @sql +\n\t\t')\n\t\tSELECT *\n\t\tFROM Custom_PageSegment\n\t\t\tWHERE\n\t\t\t" +
                      "RowIndex BETWEEN '\n\t\t\t" +
                      "+  CONVERT(nvarchar(10),@CurrentRowIndex)\n\t\t\t" +
                      "+ ' AND (' \n\t\t\t" +
                      "+ CONVERT(nvarchar(10),@CurrentRowIndex) + ' + ' + CONVERT(nvarchar(10),@PageSize)\n\t\t\t" +
                      "+ ') - 1  order by '\n\t\t\t" +
                      "+ CONVERT(nvarchar(500),@SortOrder);\n\t";

            script += "SELECT @paramlist = \n\t\t'";

            var firstItem = true;
            foreach (Column tableProparty in tableProparties)
            {
                if (firstItem)
                {
                    script += "@x" + tableProparty.Name + "[" + tableProparty.DataType + "]\n\t\t";
                    firstItem = false;
                }
                else
                {
                    if (tableProparty.DataType.ToString() == "nvarchar")
                    {
                        script += ",@x" + tableProparty.Name + "[" + tableProparty.DataType + "](max)\n\t\t";
                    }
                    else
                    {
                        script += ",@x" + tableProparty.Name + "[" + tableProparty.DataType + "]\n\t\t";
                    }
                }
            }
            script += "'EXEC sp_executesql @sql, @paramlist, ";
            firstItem = true;
            foreach (Column tableProparty in tableProparties)
            {
                if (firstItem)
                {
                    script += "\n\t\t\t@" + tableProparty.Name;
                    firstItem = false;
                }
                else
                {
                    script += "\n\t\t\t,@" + tableProparty.Name;
                }
            }
            script += "\nEND";
            storedProcedure.TextBody = script;
            try
            {
                storedProcedure.Create();
            }
            catch (Exception e)
            {
                throw new Exception("StoredProcedure Creation Error", e);
            }
        }
        private void GenerateTotalCountScript(string tableName)
        {
            var storedProcedure =
                new Microsoft.SqlServer.Management.Smo.StoredProcedure(_settings.Database, tableName + "_TotalCount" + tableName, "dbo");

            if (_settings.Database.StoredProcedures.Contains(storedProcedure.Name))
            {
                storedProcedure.Refresh();
                storedProcedure.Drop();
            }
            StoredProcedureParameter param;

            storedProcedure =
                new Microsoft.SqlServer.Management.Smo.StoredProcedure(_settings.Database, tableName + "_TotalCount" + tableName, "dbo");

            storedProcedure.TextMode = false;
            storedProcedure.AnsiNullsStatus = false;
            storedProcedure.QuotedIdentifierStatus = false;

            param = new StoredProcedureParameter(storedProcedure, "@TotalCount", DataType.Int);
            param.IsOutputParameter = true;
            storedProcedure.Parameters.Add(param);

            var script = "BEGIN\nSET NOCOUNT ON\n\tSELECT\n\t@TotalCount = COUNT(*)\n\t" +
                      "FROM [dbo].[" + tableName + "]\nEND";
            storedProcedure.TextBody = script;
            try
            {
                storedProcedure.Create();
            }
            catch (Exception e)
            {
                throw new Exception("StoredProcedure Creation Error", e);
            }
        }
        private void GenerateTotalCountWithSearchScript(string tableName)
        {
            ColumnCollection tableProparties = _settings.Server.Databases[_settings.DatabaseName].Tables[tableName, "dbo"].Columns;

            var storedProcedure =
                new Microsoft.SqlServer.Management.Smo.StoredProcedure(_settings.Database, tableName + "_TotalCountWithSearch" + tableName, "dbo");

            if (_settings.Database.StoredProcedures.Contains(storedProcedure.Name))
            {
                storedProcedure.Refresh();
                storedProcedure.Drop();
            }
            StoredProcedureParameter param;

            storedProcedure =
                new Microsoft.SqlServer.Management.Smo.StoredProcedure(_settings.Database, tableName + "_TotalCountWithSearch" + tableName, "dbo");

            storedProcedure.TextMode = false;
            storedProcedure.AnsiNullsStatus = true;
            storedProcedure.QuotedIdentifierStatus = false;

            param = new StoredProcedureParameter(storedProcedure, "@TotalCount", DataType.Int);
            param.IsOutputParameter = true;
            storedProcedure.Parameters.Add(param);

            SetParametersForSearch(_tableProparties, storedProcedure);

            var script = "BEGIN\n\tSET NOCOUNT ON\n\n\tDECLARE @sql nvarchar(max),\n\t" +
                "@paramlist  nvarchar(4000),\n\t@xTotalCount int\n\n\t";

           script +=
                "SET @sql='SELECT " +
                "@xTotalCount = COUNT(*)" +
                "FROM [dbo].[" + tableName + "] sm " +
                "WHERE 1=1'";

            //TO DO:LOOP parameters
            foreach (Column tableProparty in tableProparties)
            {
                if (tableProparty.DataType.ToString() == "nvarchar")
                {
                    script += "IF @" + tableProparty.Name +
                        " IS NOT NULL\n\t\tSET @sql = @sql + ' AND sm." + tableProparty.Name +
                        "LIKE ''%'' + @x" + tableProparty.Name + "+ ''%'''\n\t\t";
                }
                else
                {
                    script += "IF @" + tableProparty.Name +
                        " IS NOT NULL\n\t\tSET @sql = @sql + ' AND sm." + tableProparty.Name +
                        "= @x" + tableProparty.Name + "'\n\t\t";
                }

            }
            //---------------------

            script += "SELECT @paramlist = '@xTotalCount [int] OUTPUT, \n\t\t";

            var firstItem = true;
            foreach (Column tableProparty in tableProparties)
            {
                if (firstItem)
                {
                    script += "@x" + tableProparty.Name + "[" + tableProparty.DataType + "]\n\t\t";
                    firstItem = false;
                }
                else
                {
                    if (tableProparty.DataType.ToString() == "nvarchar")
                    {
                        script += ",@x" + tableProparty.Name + "[" + tableProparty.DataType + "](max)\n\t\t";
                    }
                    else
                    {
                        script += ",@x" + tableProparty.Name + "[" + tableProparty.DataType + "]\n\t\t";
                    }
                }
            }
            script += "'EXEC sp_executesql @sql, @paramlist, @xTotalCount OUTPUT,";
            firstItem = true;
            foreach (Column tableProparty in tableProparties)
            {
                if (firstItem)
                {
                    script += "\n\t\t\t@" + tableProparty.Name;
                    firstItem = false;
                }
                else
                {
                    script += "\n\t\t\t,@" + tableProparty.Name;
                }
            }
            script += "SET @TotalCount = @xTotalCount;";
            script += "\nEND";
            storedProcedure.TextBody = script;
            try
            {
                storedProcedure.Create();
            }
            catch (Exception e)
            {
                throw new Exception("StoredProcedure Creation Error", e);
            }
        }


        public void GenerateScripts(string tableName)
        {
            GenerateAddItemScript(tableName);
            GenerateUpdateItemScript(tableName);
            GenerateDeleteItemScript(tableName);
            GenerateGetItemScript(tableName);
            GenerateGetAllItemScript(tableName);
            GenerateGetAllPagedItemScript(tableName);
            GenerateTotalCountScript(tableName);
            GenerateTotalCountWithSearchScript(tableName);
        }
    }
}
