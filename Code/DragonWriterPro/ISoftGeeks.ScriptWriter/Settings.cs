using System.Collections.Generic;
using Microsoft.SqlServer.Management.Smo;

namespace ISoftGeeks.ScriptWriter
{
    public class Settings
    {
        public string Namespace { get; set; }
        public string WebNamespace { get; set; }
        public string CodeBasePath { get; set; }
        public string WebSolutionPath { get; set; }

        public string ServerName { get; set; }
        public string DatabaseName { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public Server Server { get; set; }
        public Database Database { get; set; }

        private readonly StoredProcedureGenerator _storedProcedureGenerator;
        private readonly EntityGenerator _entityGenerator;
        private readonly RepositoryGenerator _repositoryGenerator;
        private readonly ModelGenerator _modelGenerator;
        private readonly ServiceGenerator _serviceGenerator;

        public Settings()
        {
            _storedProcedureGenerator = new StoredProcedureGenerator(this);
            _entityGenerator = new EntityGenerator(this);
            _repositoryGenerator = new RepositoryGenerator(this);
            _modelGenerator = new ModelGenerator(this);
            _serviceGenerator = new ServiceGenerator(this);
        }

        public void SetServer()
        {
            Server = new Server(ServerName);
            Server.ConnectionContext.LoginSecure = false;
            Server.ConnectionContext.Login = Login;
            Server.ConnectionContext.Password = Password;
        }
        public void SetDatabase()
        {
            Database = Server.Databases[DatabaseName];
        }

        public void GenerateItems(List<Table> tables)
        {
            foreach (var table in tables)
            {
                if (table.IsChecked)
                {
                    _storedProcedureGenerator.GenerateScripts(table.Name);
                    _entityGenerator.GenerateEntity(table.Name);
                    _repositoryGenerator.GenerateRepository(table.Name);
                    _modelGenerator.GenerateModel(table.Name);
                }
            }
            _serviceGenerator.GenerateService(tables);
        }


        public void Connect()
        {
            SetServer();
            Server.ConnectionContext.Connect();
            SetDatabase();
        }
        public void Disconnect()
        {
            if (Server.ConnectionContext.IsOpen)
                Server.ConnectionContext.Disconnect();
        }
    }
}