﻿using Ninject.Modules;
using System.Configuration;

namespace DragonMember
{
    public class DragonMemberModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IEntity>().To<Entity>();
            Bind<IUser>().To<User>();

            Bind<IDragonMemberService>().To<DragonMemberService>();

            Bind<IUserRepository<User>>().To<UserRepository<User>>().WithConstructorArgument("dbConnection", ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
        }
    }
}
