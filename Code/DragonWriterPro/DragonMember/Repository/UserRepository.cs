using ISoftGeeks.Data;
using System.Collections.Generic;
using System.Linq;
namespace DragonMember
{
    public class UserRepository<TEntity> : Repository<TEntity>, IUserRepository<TEntity>
    {
        public UserRepository(string dbConnection) : base(dbConnection) { }

        public User GetUserByEmail(string email)
        {
            var parameters = new { Email = email };
            return ExecuteStoreProcidure<User>("Custom_User_GetUserByEmail", parameters).FirstOrDefault();//TODO
        }
    }
}