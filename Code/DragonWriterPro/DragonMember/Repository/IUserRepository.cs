using ISoftGeeks.Data;
namespace DragonMember
{
    public interface IUserRepository<TEntity> : IRepository<TEntity>
    {
        User GetUserByEmail(string email);
    }
}