using System;
using System.Collections.Generic;
using ISoftGeeks.Utility;
using Ninject;

namespace DragonMember
{
    public class DragonMemberService : IDragonMemberService
    {
        private Logger _logger;
        IUserRepository<User> _userRepository;

        [Inject]
        public DragonMemberService(IUserRepository<User> userRepository)
        {
            _userRepository = userRepository;
            _logger = new Logger();
        }

        #region User
        public void AddUser(User user)
        {
            try
            {
                _userRepository.AddItem(user);
            }
            catch (Exception exception)
            {
                _logger.WriteLog("Cannot insert data \n-----------------\n" + exception);
                throw;
            }
        }

        public void EditUser(User user)
        {
            try
            {
                _userRepository.UpdateItem(user);
            }
            catch (Exception exception)
            {
                _logger.WriteLog("Cannot Edit data \n-----------------\n" + exception);
                throw;
            }
        }

        public void DeleteUser(Guid id)
        {
            try
            {
                _userRepository.DeleteItem(id);
            }
            catch (Exception exception)
            {
                _logger.WriteLog("Cannot Delete data \n-----------------\n" + exception);
                throw;
            }
        }

        public IEnumerable<User> GetAllUser()
        {
            try
            {
                return _userRepository.GetAllItem();
            }
            catch (Exception exception)
            {
                _logger.WriteLog("Cannot GetAll data \n-----------------\n" + exception);
                throw;
            }
        }

        public IEnumerable<User> GetAllPagedUser(int pageSize, int pageNumber)
        {
            try
            {
                return _userRepository.GetPagedAllItem(pageSize, pageNumber);
            }
            catch (Exception exception)
            {
                _logger.WriteLog("Cannot GetAllPaged data \n-----------------\n" + exception);
                throw;
            }
        }

        public User GetUser(Guid id)
        {
            try
            {
                return _userRepository.GetItem(id);
            }
            catch (Exception exception)
            {
                _logger.WriteLog("Cannot Get data \n-----------------\n" + exception);
                throw;
            }
        }

        public int GetTotalUser()
        {
            try
            {
                return _userRepository.GetTotalCount();
            }
            catch (Exception exception)
            {
                _logger.WriteLog("Cannot GetTotal data \n-----------------\n" + exception);
                throw;
            }
        }

        public IEnumerable<User> GetAllPagedUserWithSearch(int pageSize, int pageNumber, User user, string sortOrder)
        {
            try
            {
                return _userRepository.GetPagedAllItem(pageSize, pageNumber, user, sortOrder);
            }
            catch (Exception exception)
            {
                _logger.WriteLog("Cannot GetAllPaged data \n-----------------\n" + exception);
                throw;
            }
        }

        public int GetTotalUserWithSearch(User user, string sortOrder)
        {
            try
            {
                return _userRepository.GetTotalCount(user, sortOrder);
            }
            catch (Exception exception)
            {
                _logger.WriteLog("Cannot GetTotal data WithSearch \n-----------------\n" + exception);
                throw;
            }
        }

        #endregion

        public User GetUserByEmail(string email)
        {
            if (string.IsNullOrEmpty(email))
                throw new ArgumentException("Email address is missing");

            try
            {
                return _userRepository.GetUserByEmail(email);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

    }
}