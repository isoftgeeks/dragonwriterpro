using System;
using System.Collections.Generic;

namespace DragonMember
{
    public interface IDragonMemberService
    {
        #region User
        void AddUser(User user);
        void EditUser(User user);
        void DeleteUser(Guid id);
        User GetUser(Guid id);
        IEnumerable<User> GetAllUser();
        IEnumerable<User> GetAllPagedUser(int pageSize, int pageNumber);
        int GetTotalUser();
        IEnumerable<User> GetAllPagedUserWithSearch(int pageSize, int pageNumber, User user, string sortOrder);
        int GetTotalUserWithSearch(User user, string sortOrder);
        #endregion
        User GetUserByEmail(string email);
    }
}