using System;
namespace DragonMember
{
	 public interface IEntity
	{
		Guid ID { get; set; }
		DateTime CreatedOn { get; set; }
	}
}