﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DragonMember
{
    public interface IContact:IEntity
    {
        string Name { get; set; }
        string Address { get; set; }
        string Email { get; set; }
        string Phone { get; set; }
    }
}
