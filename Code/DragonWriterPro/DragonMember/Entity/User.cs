using ISoftGeeks.Encryption;
using System;

namespace DragonMember
{
	public class User : Entity, IUser
	{
		public string EmailAddress { get; set; }
		public string Name { get; set; }
		public string Password { get; set; }
		public string Salt { get; set; }
		public string ImagePath { get; set; }
		public string ThambnailPath { get; set; }



        public int UserType { get; set; }
    }
}