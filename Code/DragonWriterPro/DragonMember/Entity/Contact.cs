﻿namespace DragonMember
{
    public class Contact : Entity,IContact
    {
        public string Name { get; set; }

        public string Address { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }
    }
}