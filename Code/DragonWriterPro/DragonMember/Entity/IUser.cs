using System;
namespace DragonMember
{
	public interface IUser : IEntity
	{
		string EmailAddress { get; set; }
		string Name { get; set; }
		string Password { get; set; }
		string Salt { get; set; }
		string ImagePath { get; set; }
		string ThambnailPath { get; set; }

        int UserType { get; set; }
		
	}
}